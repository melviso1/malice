// {user.before.include.begin}
// {user.before.include.end}

#include "malice.h"

#include <assert.h>

// {user.before.class.level.begin}
// {user.before.class.level.end}

level::node_iterator* level::node_iterator::_first = nullptr;
level::node_iterator* level::node_iterator::_last = nullptr;
size_t level::_index_level = 0;


void level::__exit__()
{
	//init from relations:
	
	    if (_ref_active_node)
	    {
	        remove_active_node(_ref_active_node);
	    }        
	
	
	    {
	        for (node* item = get_first_node(); item; item = get_first_node())
	        {
	            delete item;
	        }
	    }
	
	
	    if (_ref_super_level)
	    {
	        delete _ref_super_level;
	    }
	
	//init to relations:
}
level::level(std::fstream& f, bool override)
:	_ref_active_node(nullptr)
,	_first_node(nullptr)
,	_last_node(nullptr)
,	_count_node(0)
,	_ref_super_level(nullptr)
,	_index()
{
	f >> _index;
	if( override )
	{
		return;
	}
	load_nodes(f);
	load_nodes_relations(f);
	load_active_node(f);
	load_super_level(f);
}
level::level()
:	_ref_active_node(nullptr)
,	_first_node(nullptr)
,	_last_node(nullptr)
,	_count_node(0)
,	_ref_super_level(nullptr)
,	_index(level::_index_level++)
{
}


void level::add_active_node(node* item)
{
	
	    assert(this);
	    assert(_ref_active_node == nullptr);
	    assert(item);
	    assert(item->_ref_active_level == nullptr);
	
	    item->_ref_active_level = this;
	    _ref_active_node = item;
}


void level::remove_active_node(node* item)
{
	
	    assert(this);
	    assert(_ref_active_node == item);
	    assert(item);
	    assert(item->_ref_active_level == this);
	
	    item->_ref_active_level = nullptr;
	    _ref_active_node = nullptr;
}


void level::replace_active_node(node* item, node* new_item)
{
	
	    assert(this);
	    assert(_ref_active_node == item);
	    assert(item);
	    assert(item->_ref_active_level == this);
	    assert(new_item);
	    assert(new_item->_ref_active_level == nullptr);
	
	    item->_ref_active_level = nullptr;
	    new_item->_ref_active_level = this;
	    _ref_active_node = new_item;
	    
}


void level::move_active_node(node* item)
{
	
	    assert(item);
	    assert(item->_ref_active_level);
	    item->_ref_active_level->remove_active_node(item);
	    add_active_node(item);
}


void level::add_node_first(node* item)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_level == nullptr);
	    _count_node++;
	    item->_ref_level = this;
	    if (_first_node)
	    {
	        _first_node->_prev_level = item;
	        item->_next_level = _first_node;
	        _first_node = item;
	    }
	    else
	    {
	        _first_node = _last_node = item;
	    }
}


void level::add_node_last(node* item)
{
	
	    assert(this);
	    assert(item);
	    assert(item->_ref_level == nullptr);
	    _count_node++;
	    item->_ref_level = this;
	    if (_last_node)
	    {
	        _last_node->_next_level = item;
	        item->_prev_level = _last_node;
	        _last_node = item;
	    }
	    else
	    {
	       _first_node = _last_node = item;
	    }
}


void level::add_node_after(node* item, node* pos)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_level == nullptr);
	
	    assert(pos);
	    assert(pos->_ref_level == this);
	
	    _count_node++;
	
	    item->_ref_level = this;
	    item->_prev_level = pos;
	    item->_next_level = pos->_next_level;
	    pos->_next_level  = item;
	
	    if (item->_next_level)
	    {
	        item->_next_level->_prev_level = item;
	    }
	    else
	    {
	        _last_node = item;
	    }
}


void level::add_node_before(node* item, node* pos)
{
	
	    assert( this );
	
	    assert( item );
	    assert( item->_ref_level == nullptr );
	
	    assert( pos );
	    assert(pos->_ref_level == this);
	
	    _count_node++;
	
	    item->_ref_level = this;
	    item->_next_level = pos;
	    item->_prev_level = pos->_prev_level;
	    pos->_prev_level  = item;
	
	    if (item->_prev_level)
	    {
	        item->_prev_level->_next_level = item;
	    }
	    else
	    {
	        _first_node = item;
	    }
}


void level::remove_node(node* item)
{
	
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_level == this);
	
	    level::node_iterator::check(item);
	
	    _count_node--;
	
	    if (item->_next_level)
	    {
	        item->_next_level->_prev_level = item->_prev_level;
	    }
	    else
	    {
	        _last_node = item->_prev_level;
	    }
	
	    if (item->_prev_level)
	    {
	        item->_prev_level->_next_level = item->_next_level;
	    }
	    else
	    {
	        _first_node = item->_next_level;
	    }
	
	    item->_prev_level = nullptr;
	    item->_next_level = nullptr;
	    item->_ref_level = nullptr;
}


void level::replace_node(node* item, node* new_item)
{
	
	    assert(this);
	    assert(item);
	    assert(item->_ref_level == this);
	
	    assert(new_item);
	    assert(new_item->_ref_level == nullptr);
	
	    level::node_iterator::check(item, new_item);
	
	    if (item->_next_level)
	    {
	        item->_next_level->_prev_level = new_item;
	    }
	    else
	    {
	        _last_node = new_item;
	    }
	    if (item->_prev_level)
	    {
	        item->_prev_level->_next_level = new_item;
	    }
	    else
	    {
	        _first_node = new_item;
	    }
	
	    new_item->_next_level = item->_next_level;
	    new_item->_prev_level = item->_prev_level;
	    item->_next_level = nullptr;
	    item->_prev_level = nullptr;
	
	    item->_ref_level = nullptr;
	    new_item->_ref_level = this;
}


void level::delete_all_node()
{
	
	    assert(this);
	    for (node* item = get_first_node(); item; item = get_first_node())
	    {
	          delete item;
	    }
}


node* level::get_first_node() const
{
	
	    assert(this);
	    return _first_node;
}


node* level::get_last_node() const
{
	
	    assert(this);
	    return _last_node;
}


node* level::get_next_node(node* pos) const
{
	
	    assert(this);
	    if ( pos == nullptr )
	    {
	        return _first_node;
	    }
	    assert(pos);
	    assert(pos->_ref_level == this);
	    return pos->_next_level;
}


node* level::get_prev_node(node* pos) const
{
	
	    assert(this);
	
	    if ( pos == nullptr )
	    {
	        return _last_node;
	    }
	
	    assert(pos);
	    assert(pos->_ref_level == this);
	    return pos->_prev_level;
}


int level::get_node_count()
{
	
	    assert(this);
	    return _count_node;
}


void level::move_node_first(node* item)
{
	
	    assert(item);
	    assert(item->_ref_level);
	    item->_ref_level->remove_node(item);
	    add_node_first(item);
}


void level::move_node_last(node* item)
{
	
	    assert(item);
	    assert(item->_ref_level);
	    item->_ref_level->remove_node(item);
	    add_node_last(item);
}


void level::move_node_after(node* item, node* pos)
{
	
	    assert(item);
	    assert(item->_ref_level);
	    item->_ref_level->remove_node(item);
	    add_node_after(item, pos);
}


void level::move_node_before(node* item, node* pos)
{
	
	    assert(item);
	    assert(item->_ref_level);
	    item->_ref_level->remove_node(item);
	    add_node_before(item, pos);
	    
}


void level::sort_node(int (*compare)(node*,node*))
{
	
	
	    for (node* a = get_first_node(); a != nullptr ; a = get_next_node(a))
	    {
	        node* b = get_next_node(a);
	
	        while ( b != nullptr && compare(a, b) > 0 )
	        {
	            node* c = get_prev_node(a);
	            while ( c != nullptr  && compare(c, b) > 0 )
	                c = get_prev_node(c);
	            if (c != nullptr )
	            {
	                move_node_after(b, c);
	            }
	            else
	            {
	                move_node_first(b);
	            }
	            b = get_next_node(a);
	        }
	    }
}


void level::add_super_level(super_level* item)
{
	
	    assert(this);
	    assert(_ref_super_level == nullptr);
	    assert(item);
	    assert(item->_ref_level == nullptr);
	
	    item->_ref_level = this;
	    _ref_super_level = item;
}


void level::remove_super_level(super_level* item)
{
	
	    assert(this);
	    assert(_ref_super_level == item);
	    assert(item);
	    assert(item->_ref_level == this);
	
	    item->_ref_level = nullptr;
	    _ref_super_level = nullptr;
}


void level::replace_super_level(super_level* item, super_level* new_item)
{
	
	    assert(this);
	    assert(_ref_super_level == item);
	    assert(item);
	    assert(item->_ref_level == this);
	    assert(new_item);
	    assert(new_item->_ref_level == nullptr);
	
	    item->_ref_level = nullptr;
	    new_item->_ref_level = this;
	    _ref_super_level = new_item;
	    
}


void level::move_super_level(super_level* item)
{
	
	    assert(item);
	    assert(item->_ref_level);
	    item->_ref_level->remove_super_level(item);
	    add_super_level(item);
}


node* level::find_node(size_t index)
{
	node_iterator iter_node = node_iterator(this);
	while(++iter_node)
	{
		if(iter_node->get_index()==index)
		{
			return iter_node;
		}
	}
	return nullptr;
}


void level::set_active_node(node* p_node)
{
	node *p_node_old = get_active_node();
	if(  p_node_old != nullptr )
	{
		remove_active_node(p_node_old);
	}
	add_active_node(p_node);
}


void level::save_super_level(std::fstream& f)
{
	if(get_super_level())
	{
		f << true;
		get_super_level()->save(f);
	}
	else
	{
		f<< false;
	}
}


void level::save_active_node(std::fstream& f)
{
	if( get_active_node() != nullptr )
	{
		f << true;
		f << get_active_node()->get_index();
	}
	else
	{
		f << false;
	}
}


void level::save_nodes_relations(std::fstream& f)
{
	node_iterator iter_node = node_iterator(this);
	while(++iter_node)
	{
		iter_node->save_relations(f);
	}
}


void level::save_nodes(std::fstream& f)
{
	f << get_node_count();
	node_iterator iter_node = node_iterator(this);
	while(++iter_node)
	{
		f << iter_node->get_index();
	}
}


void level::save(std::fstream& f)
{
	
	f << get_index();
	save_nodes(f);
	save_nodes_relations(f);
	save_active_node(f);
	save_super_level(f);
}


void level::load_super_level(std::fstream& f)
{
	bool has_super_level;
	f >> has_super_level;
	if(has_super_level)
	{
		new super_level(this, f);
	}
}


void level::load_active_node(std::fstream& f)
{
	bool has_active_node;
	f >> has_active_node;
	if( has_active_node )
	{
		size_t node_index;
		f >> node_index;
		add_active_node(find_node(node_index));
	}
}


void level::cleanup()
{
	delete_all_node();
	if( get_super_level() )
	{
		delete get_super_level();
	}
}


void level::load_nodes_relations(std::fstream& f)
{
	node_iterator iter_node = node_iterator(this);
	while(++iter_node)
	{
		iter_node->load_relations(f);
	}
}


/**
* \details Load the symbols with their indexes. 
**/
void level::load_nodes(std::fstream& f)
{
	size_t node_count;
	f >> node_count;
	while(node_count--)
	{
		size_t node_index;
		f >> node_index;
		new node(this, node_index);
	}
}


void level::load(std::fstream& f)
{
	cleanup();
	f >> _index;
	load_nodes(f);
	load_nodes_relations(f);
	load_active_node(f);
	bool has_super_level;
	f >> has_super_level;
	if(has_super_level)
	{
		new super_level(this, f);
	}
}


/**
* \details Return the expected next active node from the current active node or nullptr if it cant be determined.
**/
node* level::candidate()
{
	node *p_node = get_active_node();
	if( p_node != nullptr )
	{
		return p_node->candidate();
	}
	return nullptr;
}


super_node* level::super_candidate()
{
	super_level *p_super_level = get_super_level();
	if( p_super_level != nullptr )
	{
		return p_super_level->candidate();
	}
	return nullptr;
}


/**
* \details This method does recursive candidate acativation as expected by learned paths. The activation is propagated filtered by the nested nodes ownership.
**/
node* level::activate_candidate()
{
	node *p_node = candidate();
	if( p_node != nullptr )
	{
		set_active_node(p_node);
		super_node *p_super_node = super_candidate();
		if( p_super_node != nullptr && p_super_node->get_node() == p_node )
		{
			get_super_level()->activate_candidate();
		}
	}
	return p_node;
}
level::~level()
{
		__exit__();
}
// {user.before.class.node_iterator.begin}
// {user.before.class.node_iterator.end}

level::node_iterator::node_iterator(const level* iter_level, bool(node::*method)() const, node* ref_node)
:	_ref_node(nullptr)
,	_prev_node(nullptr)
,	_next_node(nullptr)
,	_iter_level(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    assert(iter_level);
	 
	    _iter_level = iter_level;
	    _ref_node = _prev_node = _next_node = ref_node;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if ( _last != nullptr )
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
level::node_iterator::node_iterator(const level& iter_level, bool(node::*method)() const, node* ref_node)
:	_ref_node(nullptr)
,	_prev_node(nullptr)
,	_next_node(nullptr)
,	_iter_level(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	        
	    assert(&iter_level);
	 
	    _iter_level = &iter_level;
	    _ref_node = _prev_node = _next_node = ref_node;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
level::node_iterator::node_iterator(const level::node_iterator& iter, bool(node::*method)() const)
:	_ref_node(nullptr)
,	_prev_node(nullptr)
,	_next_node(nullptr)
,	_iter_level(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    _iter_level = iter._iter_level;
	    _ref_node = iter._ref_node;
	    _prev_node = iter._prev_node;
	    _next_node = iter._next_node;
	    _prev = (node_iterator*)0;
	    _next = (node_iterator*)0;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}


void level::node_iterator::check(node* item_node)
{
	
	    for (node_iterator* item = _first; item; item = item->_next)
	    {
	        if (item->_prev_node == item_node)
	        {
	            item->_prev_node = item->_iter_level->get_next_node(item->_prev_node);
	            item->_ref_node = nullptr;
	        }
	        if (item->_next_node == item_node)
	        {
	            item->_next_node = item->_iter_level->get_prev_node(item->_next_node);
	            item->_ref_node = nullptr;
	        }
	    }
}


void level::node_iterator::check(node* item_node, node* new_item_node)
{
	
	    for (node_iterator* item = _first; item != nullptr ; item = item->_next)
	    {
	        if (item->_ref_node == item_node)
	        {
	            item->_ref_node =             item->_prev_node =             item->_next_node = new_item_node;
	        }
	        if (item->_prev_node == item_node)
	        {
	            item->_prev_node = new_item_node;
	            item->_ref_node = nullptr;
	        }
	        if (item->_next_node == item_node)
	        {
	            item->_next_node = new_item_node;
	            item->_ref_node = nullptr;
	        }
	    }
}
level::node_iterator::~node_iterator()
{
	
	    if (_next)
	    {
	        _next->_prev = _prev;
	    }
	    else
	    {
	        _last = _prev;
	    }
	 
	    if (_prev)
	    {
	        _prev->_next = _next;
	    }
	    else
	    {
	        _first = _next;
	    }
}

// {user.after.class.node_iterator.begin}
// {user.after.class.node_iterator.end}


// {user.after.class.level.begin}
// {user.after.class.level.end}


