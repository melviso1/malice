// {user.before.include.begin}
// {user.before.include.end}

#include "malice.h"

// {user.before.code.begin}
#include <sstream>
// {user.before.code.end}



int main(int argc, char* argv[])
{
	interface_level interfaz;
	while(true)
	{
		std::string in;
		std::cin >> in;
		if( in == ".quit")
		{
			break;
		}
		for(char ch:in)
		{
			if(ch=='\r'||ch=='\n')
				continue;
			interfaz.set(ch);
		}
		in.clear();
		interfaz.set(0);
		size_t len = 0;
		char c;
		while( interfaz.get(c) && c!=0 && len++<1024)
		{
			std::cout << c;
		}
		if(c!=0)
			interfaz.set(0);		
	}
	return 0;
}

// {user.after.code.begin}
// {user.after.code.end}

