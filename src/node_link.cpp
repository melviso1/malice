// {user.before.include.begin}
// {user.before.include.end}

#include "malice.h"

#include <assert.h>

// {user.before.class.node_link.begin}
// {user.before.class.node_link.end}



void node_link::__init__(node* ptr_from_node, node* ptr_to_node)
{
	//init from relations:
	
	//init to relations:
	
	    //multi_owned_passive from_node -> to_node
	    {
	        assert(this);
	        assert(ptr_from_node);
	        ptr_from_node->_count_to_node++;
	    
	        _ref_from_node = ptr_from_node;
	    
	        if (ptr_from_node->_last_to_node)
	        {
	            _next_from_node = nullptr;
	            _prev_from_node = ptr_from_node->_last_to_node;
	            _prev_from_node->_next_from_node = this;
	            ptr_from_node->_last_to_node = this;
	        }
	        else
	        {
	            _prev_from_node = nullptr;
	            _next_from_node = nullptr;
	            ptr_from_node->_first_to_node = ptr_from_node->_last_to_node = this;
	        }
	    }
	
	
	    //multi_owned_passive to_node -> from_node
	    {
	        assert(this);
	        assert(ptr_to_node);
	        ptr_to_node->_count_from_node++;
	    
	        _ref_to_node = ptr_to_node;
	    
	        if (ptr_to_node->_last_from_node)
	        {
	            _next_to_node = nullptr;
	            _prev_to_node = ptr_to_node->_last_from_node;
	            _prev_to_node->_next_to_node = this;
	            ptr_to_node->_last_from_node = this;
	        }
	        else
	        {
	            _prev_to_node = nullptr;
	            _next_to_node = nullptr;
	            ptr_to_node->_first_from_node = ptr_to_node->_last_from_node = this;
	        }
	    }
}


void node_link::__exit__()
{
	//init from relations:
	
	//init to relations:
	
	    assert(this);
	    if (_ref_from_node)
	    {
	        node::to_node_iterator::check(this);
	
	        _ref_from_node->_count_to_node--;
	
	        if (_next_from_node)
	        {
	            _next_from_node->_prev_from_node = _prev_from_node;
	        }
	        else
	        {
	            _ref_from_node->_last_to_node = _prev_from_node;
	        }
	
	        if (_prev_from_node)
	        {
	            _prev_from_node->_next_from_node = _next_from_node;
	        }
	        else
	        {
	            _ref_from_node->_first_to_node = _next_from_node;
	        }
	        _prev_from_node = nullptr;
	        _next_from_node = nullptr;
	        _ref_from_node = nullptr;
	    }
	
	
	    assert(this);
	    if (_ref_to_node)
	    {
	        node::from_node_iterator::check(this);
	
	        _ref_to_node->_count_from_node--;
	
	        if (_next_to_node)
	        {
	            _next_to_node->_prev_to_node = _prev_to_node;
	        }
	        else
	        {
	            _ref_to_node->_last_from_node = _prev_to_node;
	        }
	
	        if (_prev_to_node)
	        {
	            _prev_to_node->_next_to_node = _next_to_node;
	        }
	        else
	        {
	            _ref_to_node->_first_from_node = _next_to_node;
	        }
	        _prev_to_node = nullptr;
	        _next_to_node = nullptr;
	        _ref_to_node = nullptr;
	    }
}
node_link::node_link(node* ptr_from_node, node* ptr_to_node)
:	_ref_to_node(nullptr)
,	_prev_to_node(nullptr)
,	_next_to_node(nullptr)
,	_ref_from_node(nullptr)
,	_prev_from_node(nullptr)
,	_next_from_node(nullptr)
{
		__init__( ptr_from_node, ptr_to_node );
		if (! ptr_to_node->is_from(ptr_from_node) )
		{
			std::cout << "la construccion de enlaces tiene algun problema." << std::endl;
		}
		
}
node_link::~node_link()
{
		__exit__();
}

// {user.after.class.node_link.begin}
// {user.after.class.node_link.end}


