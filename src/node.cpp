// {user.before.include.begin}
#include <random>
// {user.before.include.end}

#include "malice.h"

#include <assert.h>

// {user.before.class.node.begin}
// {user.before.class.node.end}

node::from_node_iterator* node::from_node_iterator::_first = nullptr;
node::from_node_iterator* node::from_node_iterator::_last = nullptr;
node::to_node_iterator* node::to_node_iterator::_first = nullptr;
node::to_node_iterator* node::to_node_iterator::_last = nullptr;
node::super_node_iterator* node::super_node_iterator::_first = nullptr;
node::super_node_iterator* node::super_node_iterator::_last = nullptr;
size_t node::_index_node = 0;


void node::__init__(level* ptr_level)
{
	//init from relations:
	
	    _first_super_node = nullptr;
	    _last_super_node = nullptr;
	    _count_super_node = 0;
	
	
	    _first_to_node = nullptr;
	    _last_to_node = nullptr;
	    _count_to_node = 0;
	
	
	    _first_from_node = nullptr;
	    _last_from_node = nullptr;
	    _count_from_node = 0;
	
	//init to relations:
	
	    //single_passive active_level -> active_node
	    _ref_active_level = nullptr;
	
	
	    //multi_owned_passive level -> node
	    {
	        assert(this);
	        assert(ptr_level);
	        ptr_level->_count_node++;
	    
	        _ref_level = ptr_level;
	    
	        if (ptr_level->_last_node)
	        {
	            _next_level = nullptr;
	            _prev_level = ptr_level->_last_node;
	            _prev_level->_next_level = this;
	            ptr_level->_last_node = this;
	        }
	        else
	        {
	            _prev_level = nullptr;
	            _next_level = nullptr;
	            ptr_level->_first_node = ptr_level->_last_node = this;
	        }
	    }
}


void node::__exit__()
{
	//init from relations:
	
	    {
	        for (super_node* item = get_first_super_node(); item; item = get_first_super_node())
	        {
	            delete item;
	        }
	    }
	
	
	    {
	        for (node_link* item = get_first_to_node(); item; item = get_first_to_node())
	        {
	            delete item;
	        }
	    }
	
	
	    {
	        for (node_link* item = get_first_from_node(); item; item = get_first_from_node())
	        {
	            delete item;
	        }
	    }
	
	//init to relations:
	
	    assert(this);
	    if (_ref_active_level)
	    {
	        assert(_ref_active_level->_ref_active_node == this);
	        _ref_active_level->_ref_active_node = nullptr;
	        _ref_active_level = nullptr;
	    }
	
	
	    assert(this);
	    if (_ref_level)
	    {
	        level::node_iterator::check(this);
	
	        _ref_level->_count_node--;
	
	        if (_next_level)
	        {
	            _next_level->_prev_level = _prev_level;
	        }
	        else
	        {
	            _ref_level->_last_node = _prev_level;
	        }
	
	        if (_prev_level)
	        {
	            _prev_level->_next_level = _next_level;
	        }
	        else
	        {
	            _ref_level->_first_node = _next_level;
	        }
	        _prev_level = nullptr;
	        _next_level = nullptr;
	        _ref_level = nullptr;
	    }
}
node::node(level* ptr_level, size_t index)
:	_ref_active_level(nullptr)
,	_ref_level(nullptr)
,	_prev_level(nullptr)
,	_next_level(nullptr)
,	_first_from_node(nullptr)
,	_last_from_node(nullptr)
,	_count_from_node(0)
,	_first_to_node(nullptr)
,	_last_to_node(nullptr)
,	_count_to_node(0)
,	_first_super_node(nullptr)
,	_last_super_node(nullptr)
,	_count_super_node(0)
,	_index(index)
{
		__init__( ptr_level );
}
node::node(level* ptr_level)
:	_ref_active_level(nullptr)
,	_ref_level(nullptr)
,	_prev_level(nullptr)
,	_next_level(nullptr)
,	_first_from_node(nullptr)
,	_last_from_node(nullptr)
,	_count_from_node(0)
,	_first_to_node(nullptr)
,	_last_to_node(nullptr)
,	_count_to_node(0)
,	_first_super_node(nullptr)
,	_last_super_node(nullptr)
,	_count_super_node(0)
,	_index(node::_index_node++)
{
		__init__( ptr_level );
}


bool node::active() const
{
	if( get_level()->get_active_node() == this )
	{
		return true;
	}
	else
	{
		return false;
	}
}


void node::add_from_node_first(node_link* item)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_to_node == nullptr);
	    _count_from_node++;
	    item->_ref_to_node = this;
	    if (_first_from_node)
	    {
	        _first_from_node->_prev_to_node = item;
	        item->_next_to_node = _first_from_node;
	        _first_from_node = item;
	    }
	    else
	    {
	        _first_from_node = _last_from_node = item;
	    }
}


void node::add_from_node_last(node_link* item)
{
	
	    assert(this);
	    assert(item);
	    assert(item->_ref_to_node == nullptr);
	    _count_from_node++;
	    item->_ref_to_node = this;
	    if (_last_from_node)
	    {
	        _last_from_node->_next_to_node = item;
	        item->_prev_to_node = _last_from_node;
	        _last_from_node = item;
	    }
	    else
	    {
	       _first_from_node = _last_from_node = item;
	    }
}


void node::add_from_node_after(node_link* item, node_link* pos)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_to_node == nullptr);
	
	    assert(pos);
	    assert(pos->_ref_to_node == this);
	
	    _count_from_node++;
	
	    item->_ref_to_node = this;
	    item->_prev_to_node = pos;
	    item->_next_to_node = pos->_next_to_node;
	    pos->_next_to_node  = item;
	
	    if (item->_next_to_node)
	    {
	        item->_next_to_node->_prev_to_node = item;
	    }
	    else
	    {
	        _last_from_node = item;
	    }
}


void node::add_from_node_before(node_link* item, node_link* pos)
{
	
	    assert( this );
	
	    assert( item );
	    assert( item->_ref_to_node == nullptr );
	
	    assert( pos );
	    assert(pos->_ref_to_node == this);
	
	    _count_from_node++;
	
	    item->_ref_to_node = this;
	    item->_next_to_node = pos;
	    item->_prev_to_node = pos->_prev_to_node;
	    pos->_prev_to_node  = item;
	
	    if (item->_prev_to_node)
	    {
	        item->_prev_to_node->_next_to_node = item;
	    }
	    else
	    {
	        _first_from_node = item;
	    }
}


void node::remove_from_node(node_link* item)
{
	
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_to_node == this);
	
	    node::from_node_iterator::check(item);
	
	    _count_from_node--;
	
	    if (item->_next_to_node)
	    {
	        item->_next_to_node->_prev_to_node = item->_prev_to_node;
	    }
	    else
	    {
	        _last_from_node = item->_prev_to_node;
	    }
	
	    if (item->_prev_to_node)
	    {
	        item->_prev_to_node->_next_to_node = item->_next_to_node;
	    }
	    else
	    {
	        _first_from_node = item->_next_to_node;
	    }
	
	    item->_prev_to_node = nullptr;
	    item->_next_to_node = nullptr;
	    item->_ref_to_node = nullptr;
}


void node::replace_from_node(node_link* item, node_link* new_item)
{
	
	    assert(this);
	    assert(item);
	    assert(item->_ref_to_node == this);
	
	    assert(new_item);
	    assert(new_item->_ref_to_node == nullptr);
	
	    node::from_node_iterator::check(item, new_item);
	
	    if (item->_next_to_node)
	    {
	        item->_next_to_node->_prev_to_node = new_item;
	    }
	    else
	    {
	        _last_from_node = new_item;
	    }
	    if (item->_prev_to_node)
	    {
	        item->_prev_to_node->_next_to_node = new_item;
	    }
	    else
	    {
	        _first_from_node = new_item;
	    }
	
	    new_item->_next_to_node = item->_next_to_node;
	    new_item->_prev_to_node = item->_prev_to_node;
	    item->_next_to_node = nullptr;
	    item->_prev_to_node = nullptr;
	
	    item->_ref_to_node = nullptr;
	    new_item->_ref_to_node = this;
}


void node::delete_all_from_node()
{
	
	    assert(this);
	    for (node_link* item = get_first_from_node(); item; item = get_first_from_node())
	    {
	          delete item;
	    }
}


node_link* node::get_first_from_node() const
{
	
	    assert(this);
	    return _first_from_node;
}


node_link* node::get_last_from_node() const
{
	
	    assert(this);
	    return _last_from_node;
}


node_link* node::get_next_from_node(node_link* pos) const
{
	
	    assert(this);
	    if ( pos == nullptr )
	    {
	        return _first_from_node;
	    }
	    assert(pos);
	    assert(pos->_ref_to_node == this);
	    return pos->_next_to_node;
}


node_link* node::get_prev_from_node(node_link* pos) const
{
	
	    assert(this);
	
	    if ( pos == nullptr )
	    {
	        return _last_from_node;
	    }
	
	    assert(pos);
	    assert(pos->_ref_to_node == this);
	    return pos->_prev_to_node;
}


int node::get_from_node_count()
{
	
	    assert(this);
	    return _count_from_node;
}


void node::move_from_node_first(node_link* item)
{
	
	    assert(item);
	    assert(item->_ref_to_node);
	    item->_ref_to_node->remove_from_node(item);
	    add_from_node_first(item);
}


void node::move_from_node_last(node_link* item)
{
	
	    assert(item);
	    assert(item->_ref_to_node);
	    item->_ref_to_node->remove_from_node(item);
	    add_from_node_last(item);
}


void node::move_from_node_after(node_link* item, node_link* pos)
{
	
	    assert(item);
	    assert(item->_ref_to_node);
	    item->_ref_to_node->remove_from_node(item);
	    add_from_node_after(item, pos);
}


void node::move_from_node_before(node_link* item, node_link* pos)
{
	
	    assert(item);
	    assert(item->_ref_to_node);
	    item->_ref_to_node->remove_from_node(item);
	    add_from_node_before(item, pos);
	    
}


void node::sort_from_node(int (*compare)(node_link*,node_link*))
{
	
	
	    for (node_link* a = get_first_from_node(); a != nullptr ; a = get_next_from_node(a))
	    {
	        node_link* b = get_next_from_node(a);
	
	        while ( b != nullptr && compare(a, b) > 0 )
	        {
	            node_link* c = get_prev_from_node(a);
	            while ( c != nullptr  && compare(c, b) > 0 )
	                c = get_prev_from_node(c);
	            if (c != nullptr )
	            {
	                move_from_node_after(b, c);
	            }
	            else
	            {
	                move_from_node_first(b);
	            }
	            b = get_next_from_node(a);
	        }
	    }
}


void node::add_to_node_first(node_link* item)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_from_node == nullptr);
	    _count_to_node++;
	    item->_ref_from_node = this;
	    if (_first_to_node)
	    {
	        _first_to_node->_prev_from_node = item;
	        item->_next_from_node = _first_to_node;
	        _first_to_node = item;
	    }
	    else
	    {
	        _first_to_node = _last_to_node = item;
	    }
}


void node::add_to_node_last(node_link* item)
{
	
	    assert(this);
	    assert(item);
	    assert(item->_ref_from_node == nullptr);
	    _count_to_node++;
	    item->_ref_from_node = this;
	    if (_last_to_node)
	    {
	        _last_to_node->_next_from_node = item;
	        item->_prev_from_node = _last_to_node;
	        _last_to_node = item;
	    }
	    else
	    {
	       _first_to_node = _last_to_node = item;
	    }
}


void node::add_to_node_after(node_link* item, node_link* pos)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_from_node == nullptr);
	
	    assert(pos);
	    assert(pos->_ref_from_node == this);
	
	    _count_to_node++;
	
	    item->_ref_from_node = this;
	    item->_prev_from_node = pos;
	    item->_next_from_node = pos->_next_from_node;
	    pos->_next_from_node  = item;
	
	    if (item->_next_from_node)
	    {
	        item->_next_from_node->_prev_from_node = item;
	    }
	    else
	    {
	        _last_to_node = item;
	    }
}


void node::add_to_node_before(node_link* item, node_link* pos)
{
	
	    assert( this );
	
	    assert( item );
	    assert( item->_ref_from_node == nullptr );
	
	    assert( pos );
	    assert(pos->_ref_from_node == this);
	
	    _count_to_node++;
	
	    item->_ref_from_node = this;
	    item->_next_from_node = pos;
	    item->_prev_from_node = pos->_prev_from_node;
	    pos->_prev_from_node  = item;
	
	    if (item->_prev_from_node)
	    {
	        item->_prev_from_node->_next_from_node = item;
	    }
	    else
	    {
	        _first_to_node = item;
	    }
}


void node::remove_to_node(node_link* item)
{
	
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_from_node == this);
	
	    node::to_node_iterator::check(item);
	
	    _count_to_node--;
	
	    if (item->_next_from_node)
	    {
	        item->_next_from_node->_prev_from_node = item->_prev_from_node;
	    }
	    else
	    {
	        _last_to_node = item->_prev_from_node;
	    }
	
	    if (item->_prev_from_node)
	    {
	        item->_prev_from_node->_next_from_node = item->_next_from_node;
	    }
	    else
	    {
	        _first_to_node = item->_next_from_node;
	    }
	
	    item->_prev_from_node = nullptr;
	    item->_next_from_node = nullptr;
	    item->_ref_from_node = nullptr;
}


void node::replace_to_node(node_link* item, node_link* new_item)
{
	
	    assert(this);
	    assert(item);
	    assert(item->_ref_from_node == this);
	
	    assert(new_item);
	    assert(new_item->_ref_from_node == nullptr);
	
	    node::to_node_iterator::check(item, new_item);
	
	    if (item->_next_from_node)
	    {
	        item->_next_from_node->_prev_from_node = new_item;
	    }
	    else
	    {
	        _last_to_node = new_item;
	    }
	    if (item->_prev_from_node)
	    {
	        item->_prev_from_node->_next_from_node = new_item;
	    }
	    else
	    {
	        _first_to_node = new_item;
	    }
	
	    new_item->_next_from_node = item->_next_from_node;
	    new_item->_prev_from_node = item->_prev_from_node;
	    item->_next_from_node = nullptr;
	    item->_prev_from_node = nullptr;
	
	    item->_ref_from_node = nullptr;
	    new_item->_ref_from_node = this;
}


void node::delete_all_to_node()
{
	
	    assert(this);
	    for (node_link* item = get_first_to_node(); item; item = get_first_to_node())
	    {
	          delete item;
	    }
}


node_link* node::get_first_to_node() const
{
	
	    assert(this);
	    return _first_to_node;
}


node_link* node::get_last_to_node() const
{
	
	    assert(this);
	    return _last_to_node;
}


node_link* node::get_next_to_node(node_link* pos) const
{
	
	    assert(this);
	    if ( pos == nullptr )
	    {
	        return _first_to_node;
	    }
	    assert(pos);
	    assert(pos->_ref_from_node == this);
	    return pos->_next_from_node;
}


node_link* node::get_prev_to_node(node_link* pos) const
{
	
	    assert(this);
	
	    if ( pos == nullptr )
	    {
	        return _last_to_node;
	    }
	
	    assert(pos);
	    assert(pos->_ref_from_node == this);
	    return pos->_prev_from_node;
}


int node::get_to_node_count()
{
	
	    assert(this);
	    return _count_to_node;
}


void node::move_to_node_first(node_link* item)
{
	
	    assert(item);
	    assert(item->_ref_from_node);
	    item->_ref_from_node->remove_to_node(item);
	    add_to_node_first(item);
}


void node::move_to_node_last(node_link* item)
{
	
	    assert(item);
	    assert(item->_ref_from_node);
	    item->_ref_from_node->remove_to_node(item);
	    add_to_node_last(item);
}


void node::move_to_node_after(node_link* item, node_link* pos)
{
	
	    assert(item);
	    assert(item->_ref_from_node);
	    item->_ref_from_node->remove_to_node(item);
	    add_to_node_after(item, pos);
}


void node::move_to_node_before(node_link* item, node_link* pos)
{
	
	    assert(item);
	    assert(item->_ref_from_node);
	    item->_ref_from_node->remove_to_node(item);
	    add_to_node_before(item, pos);
	    
}


void node::sort_to_node(int (*compare)(node_link*,node_link*))
{
	
	
	    for (node_link* a = get_first_to_node(); a != nullptr ; a = get_next_to_node(a))
	    {
	        node_link* b = get_next_to_node(a);
	
	        while ( b != nullptr && compare(a, b) > 0 )
	        {
	            node_link* c = get_prev_to_node(a);
	            while ( c != nullptr  && compare(c, b) > 0 )
	                c = get_prev_to_node(c);
	            if (c != nullptr )
	            {
	                move_to_node_after(b, c);
	            }
	            else
	            {
	                move_to_node_first(b);
	            }
	            b = get_next_to_node(a);
	        }
	    }
}


void node::add_super_node_first(super_node* item)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_node == nullptr);
	    _count_super_node++;
	    item->_ref_node = this;
	    if (_first_super_node)
	    {
	        _first_super_node->_prev_node = item;
	        item->_next_node = _first_super_node;
	        _first_super_node = item;
	    }
	    else
	    {
	        _first_super_node = _last_super_node = item;
	    }
}


void node::add_super_node_last(super_node* item)
{
	
	    assert(this);
	    assert(item);
	    assert(item->_ref_node == nullptr);
	    _count_super_node++;
	    item->_ref_node = this;
	    if (_last_super_node)
	    {
	        _last_super_node->_next_node = item;
	        item->_prev_node = _last_super_node;
	        _last_super_node = item;
	    }
	    else
	    {
	       _first_super_node = _last_super_node = item;
	    }
}


void node::add_super_node_after(super_node* item, super_node* pos)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_node == nullptr);
	
	    assert(pos);
	    assert(pos->_ref_node == this);
	
	    _count_super_node++;
	
	    item->_ref_node = this;
	    item->_prev_node = pos;
	    item->_next_node = pos->_next_node;
	    pos->_next_node  = item;
	
	    if (item->_next_node)
	    {
	        item->_next_node->_prev_node = item;
	    }
	    else
	    {
	        _last_super_node = item;
	    }
}


void node::add_super_node_before(super_node* item, super_node* pos)
{
	
	    assert( this );
	
	    assert( item );
	    assert( item->_ref_node == nullptr );
	
	    assert( pos );
	    assert(pos->_ref_node == this);
	
	    _count_super_node++;
	
	    item->_ref_node = this;
	    item->_next_node = pos;
	    item->_prev_node = pos->_prev_node;
	    pos->_prev_node  = item;
	
	    if (item->_prev_node)
	    {
	        item->_prev_node->_next_node = item;
	    }
	    else
	    {
	        _first_super_node = item;
	    }
}


void node::remove_super_node(super_node* item)
{
	
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_node == this);
	
	    node::super_node_iterator::check(item);
	
	    _count_super_node--;
	
	    if (item->_next_node)
	    {
	        item->_next_node->_prev_node = item->_prev_node;
	    }
	    else
	    {
	        _last_super_node = item->_prev_node;
	    }
	
	    if (item->_prev_node)
	    {
	        item->_prev_node->_next_node = item->_next_node;
	    }
	    else
	    {
	        _first_super_node = item->_next_node;
	    }
	
	    item->_prev_node = nullptr;
	    item->_next_node = nullptr;
	    item->_ref_node = nullptr;
}


void node::replace_super_node(super_node* item, super_node* new_item)
{
	
	    assert(this);
	    assert(item);
	    assert(item->_ref_node == this);
	
	    assert(new_item);
	    assert(new_item->_ref_node == nullptr);
	
	    node::super_node_iterator::check(item, new_item);
	
	    if (item->_next_node)
	    {
	        item->_next_node->_prev_node = new_item;
	    }
	    else
	    {
	        _last_super_node = new_item;
	    }
	    if (item->_prev_node)
	    {
	        item->_prev_node->_next_node = new_item;
	    }
	    else
	    {
	        _first_super_node = new_item;
	    }
	
	    new_item->_next_node = item->_next_node;
	    new_item->_prev_node = item->_prev_node;
	    item->_next_node = nullptr;
	    item->_prev_node = nullptr;
	
	    item->_ref_node = nullptr;
	    new_item->_ref_node = this;
}


void node::delete_all_super_node()
{
	
	    assert(this);
	    for (super_node* item = get_first_super_node(); item; item = get_first_super_node())
	    {
	          delete item;
	    }
}


super_node* node::get_first_super_node() const
{
	
	    assert(this);
	    return _first_super_node;
}


super_node* node::get_last_super_node() const
{
	
	    assert(this);
	    return _last_super_node;
}


super_node* node::get_next_super_node(super_node* pos) const
{
	
	    assert(this);
	    if ( pos == nullptr )
	    {
	        return _first_super_node;
	    }
	    assert(pos);
	    assert(pos->_ref_node == this);
	    return pos->_next_node;
}


super_node* node::get_prev_super_node(super_node* pos) const
{
	
	    assert(this);
	
	    if ( pos == nullptr )
	    {
	        return _last_super_node;
	    }
	
	    assert(pos);
	    assert(pos->_ref_node == this);
	    return pos->_prev_node;
}


int node::get_super_node_count()
{
	
	    assert(this);
	    return _count_super_node;
}


void node::move_super_node_first(super_node* item)
{
	
	    assert(item);
	    assert(item->_ref_node);
	    item->_ref_node->remove_super_node(item);
	    add_super_node_first(item);
}


void node::move_super_node_last(super_node* item)
{
	
	    assert(item);
	    assert(item->_ref_node);
	    item->_ref_node->remove_super_node(item);
	    add_super_node_last(item);
}


void node::move_super_node_after(super_node* item, super_node* pos)
{
	
	    assert(item);
	    assert(item->_ref_node);
	    item->_ref_node->remove_super_node(item);
	    add_super_node_after(item, pos);
}


void node::move_super_node_before(super_node* item, super_node* pos)
{
	
	    assert(item);
	    assert(item->_ref_node);
	    item->_ref_node->remove_super_node(item);
	    add_super_node_before(item, pos);
	    
}


void node::sort_super_node(int (*compare)(super_node*,super_node*))
{
	
	
	    for (super_node* a = get_first_super_node(); a != nullptr ; a = get_next_super_node(a))
	    {
	        super_node* b = get_next_super_node(a);
	
	        while ( b != nullptr && compare(a, b) > 0 )
	        {
	            super_node* c = get_prev_super_node(a);
	            while ( c != nullptr  && compare(c, b) > 0 )
	                c = get_prev_super_node(c);
	            if (c != nullptr )
	            {
	                move_super_node_after(b, c);
	            }
	            else
	            {
	                move_super_node_first(b);
	            }
	            b = get_next_super_node(a);
	        }
	    }
}


bool node::save_relations(std::fstream& f)
{
	f << get_to_node_count();
	to_node_iterator iter_to_node = to_node_iterator(this);
	while(++iter_to_node)
	{
		f << iter_to_node->get_to_node()->get_index();
	}
	
	return true;
}


bool node::load_relations(std::fstream& f)
{
	size_t count;
	f >> count;
	while(count--)
	{
		size_t index;
		f >> index;
		node* item = get_level()->find_node(index);
		new node_link(this, item);	
	}
	return true;
}


/**
* \details Return the candidate node, based en decission tree.
**/
node* node::candidate()
{
	if(get_to_node_count() == 0 || not active())
	{
		return nullptr;
	}
	
	super_node *p_super_candidate = get_level()->super_candidate();
	if( p_super_candidate != nullptr )
	{
		node *p_induced_candidate = p_super_candidate->get_node();
		if( p_induced_candidate->is_from(this) )
		{
			return p_induced_candidate;
		}
	}
	
	return get_first_to_node()->get_to_node();
	
}


/**
* \details Return true if the node is the candidate of the active node in the level.
* Return false if the level has not active node or if the candidate is different.
**/
bool node::is_candidate()
{
	
	node *p_node = get_level()->get_active_node();
	
	if( p_node == nullptr )
	{
		return false;
	}
	return ( p_node->candidate() == this )? true: false;
}


/**
* \details Make the node the current active node with independence from if this node was the expected candidate. This method can trigger learning and structure modification.
**/
void node::activate()
{
	#if defined(DEBUG)
	std::cout << "Activando"; who(); std::cout << std::endl;
	#endif
	node *p_node_candidate = get_level()->candidate();
	if( p_node_candidate == this )
	{
		#if defined(DEBUG)
		std::cout << "El candidato se activa como se esperaba."<< std::endl;
		#endif
		get_level()->activate_candidate();
		return;
	}
	#if defined(DEBUG)
	if(p_node_candidate==nullptr)
	{
		std::cout << "No habia ningun candidato aun. ";
	}else{
		std::cout << "El nodo no es el esperado. El esperado era"; p_node_candidate->who(); 
	}
	std::cout<< std::endl;
	#endif
	
	/**
	Learning part: As the node is not candidate,
	the structure need to be modified. There are
	several possible cases.
	**/
	
	node *p_active_node = get_level()->get_active_node();
	/**
	I.- start condition: The level is recently born
	and has not a active node. This case is easy as
	we can't link with the past. Only activate and
	go **/
	if(  p_active_node == nullptr )
	{
	#if defined(DEBUG)
		std::cout << "No habia ningun nodo activo en el nivel aun." << std::endl;
	#endif	
		get_level()->set_active_node(this);
		return;
	}
	
	super_level *p_super_level = get_level()->get_super_level();
	/**
	II.- Transmission.
	If the current active node and this node
	are linked by from->to link, the learning
	must be translated to super_level before.
	**/
	if(is_from( p_active_node ) )
	{
		/** As it exist another non null different candidate and
		the active node is linked with this, this imply
		the existence of a super level. We must find a supernode 
		from this node to get activated in superlevel.
		**/
	#if defined(DEBUG)
		std::cout << "Existe camino previo desde el nodo activo actual ("; p_active_node->who(); std::cout<< ")" << std::endl;
	#endif	
		if( p_super_level == nullptr )
		{
			std::cout << "Error: p_super_level == nullptr!!!" << std::endl;
			return;
		}
		super_node *p_super_active_node = p_super_level->get_active_node();
		if( p_super_active_node  == nullptr )
		{
			std::cout << "Error: p_super_active_node == nullptr!!!" << std::endl;
			return;
		}
		super_node_iterator iter_super_node = super_node_iterator(this);
		super_node *p_super_activate_node = nullptr;
		while(++iter_super_node)
		{
			if( iter_super_node->is_from(p_super_active_node) )
			{
				/** Ok, we found the transmission **/
				p_super_activate_node = iter_super_node;
				break;
			}
		}
		/**
		If the supernode path doesn't exist, we must create it? Well, that's a very interesting point.
		If we always create a supernode target for a new path, we never close any path. In other hand, if
		we always reuse existing supernodes, the superlevel cant hold as many nodes as base level and error
		propagation is too hard. We use OVERLOAD_LEVEL (3 by default) as the limit of supernodes for node.
		**/
		if( p_super_activate_node == nullptr )
		{
			p_super_activate_node = register_super_node();
		}
		if( p_super_activate_node  == nullptr )
		{
			std::cout << "Error: p_super_active_node == nullptr!!! (2)" << std::endl;
			return;
		}
		p_super_activate_node->activate();
		get_level()->set_active_node(this);
		return;
	}
	/**
	III.- New link
	The activation requires the creation of a new link between nodes.
	This process has the prerrequisites of information conservation
	forward and backward
	**/
	
	/**
	Backward infomation conservation
	-------------------------------
	When a node becomes active, it's necessary to know which was the 
	previous active node. As the activation implies learning, if this
	node has several from nodes, selecting back implies the necessity
	of an super active node over the active node.
	
	**/
	if( get_from_node_count() > 0 )
	{
		if( p_super_level == nullptr )
		{
			p_super_level = new super_level(get_level());
		}
		super_node *p_super_active_node = p_super_level->get_active_node();
		if( p_super_active_node == nullptr || p_super_active_node->get_node() != p_active_node )
		{
			/**
			Check if it exists some supernode activable over active node
			**/
			super_node *p_super_activate_node = nullptr;
			if( p_super_active_node != nullptr )
			{
				super_node_iterator iter_super_node = super_node_iterator(p_active_node);
				while(++iter_super_node)
				{
					if( iter_super_node->is_from(p_super_active_node) )
					{
						/** Ok, we found the transmission **/
						p_super_activate_node = iter_super_node;
						break;
					}
				}
			}
			if( p_super_activate_node == nullptr )
			{
				p_super_activate_node = p_active_node->register_super_node();
			}
			p_super_activate_node->activate();
		}
	}
	
	/**
	Forward information conservation
	---------------------------------
	When the current active node has to nodes, the target node 
	may be determined by an active node. As this is not a
	propagation case, the supernode may be created.
	**/
	
	if( p_active_node->get_to_node_count() > 0 )
	{
		register_super_node()->activate();
	}
	
	/** Ok, create the link, activate, and done ! **/
	new node_link( p_active_node, this );
	get_level()->set_active_node(this);
}


/**
* \details Return true if this node is linked from the node previous.
**/
bool node::is_from(node* p_node)
{
	//Use shortest search list
	if( get_from_node_count() <= p_node->get_to_node_count() )
	{
		from_node_iterator iter_from_node = from_node_iterator(this);
		while(++iter_from_node)
		{
			if( iter_from_node->get_from_node() == p_node )
			{
				return true;
			}
		}
	}
	else
	{
		to_node_iterator iter_to_node = to_node_iterator(p_node);
		while(++iter_to_node)
		{
			if( iter_to_node->get_to_node() == this )
			{
				return true;
			}
		}
	}
	return false;
}


/**
* \details Este metodo crea un nuevo supernodo mientras no se alcance el límite OVERLOAD_LEVEL o bien retorna uno aleatoriamente. 
* 
**/
super_node* node::register_super_node()
{
	super_level *p_super_level = get_level()->get_super_level();
	if( p_super_level == nullptr )
	{
		p_super_level = new super_level(get_level());
	}
	if( get_super_node_count() < OVERLOAD_LEVEL )
	{
		return new super_node(p_super_level, this);
	}	
	super_node_iterator iter_super_node = super_node_iterator(this);
	std::random_device rd;
	std::uniform_int_distribution<int> dist(1, 3);
	int n = dist(rd);
	while(n--)
	{
		++iter_super_node;
	}
	return iter_super_node.get();
}


void node::who()
{
	std::cout << " nodo " << this ;
}
node::~node()
{
		__exit__();
}
// {user.before.class.from_node_iterator.begin}
// {user.before.class.from_node_iterator.end}

node::from_node_iterator::from_node_iterator(const node* iter_to_node, bool(node_link::*method)() const, node_link* ref_from_node)
:	_ref_from_node(nullptr)
,	_prev_from_node(nullptr)
,	_next_from_node(nullptr)
,	_iter_to_node(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    assert(iter_to_node);
	 
	    _iter_to_node = iter_to_node;
	    _ref_from_node = _prev_from_node = _next_from_node = ref_from_node;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if ( _last != nullptr )
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
node::from_node_iterator::from_node_iterator(const node& iter_to_node, bool(node_link::*method)() const, node_link* ref_from_node)
:	_ref_from_node(nullptr)
,	_prev_from_node(nullptr)
,	_next_from_node(nullptr)
,	_iter_to_node(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	        
	    assert(&iter_to_node);
	 
	    _iter_to_node = &iter_to_node;
	    _ref_from_node = _prev_from_node = _next_from_node = ref_from_node;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
node::from_node_iterator::from_node_iterator(const node::from_node_iterator& iter, bool(node_link::*method)() const)
:	_ref_from_node(nullptr)
,	_prev_from_node(nullptr)
,	_next_from_node(nullptr)
,	_iter_to_node(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    _iter_to_node = iter._iter_to_node;
	    _ref_from_node = iter._ref_from_node;
	    _prev_from_node = iter._prev_from_node;
	    _next_from_node = iter._next_from_node;
	    _prev = (from_node_iterator*)0;
	    _next = (from_node_iterator*)0;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}


void node::from_node_iterator::check(node_link* item_from_node)
{
	
	    for (from_node_iterator* item = _first; item; item = item->_next)
	    {
	        if (item->_prev_from_node == item_from_node)
	        {
	            item->_prev_from_node = item->_iter_to_node->get_next_from_node(item->_prev_from_node);
	            item->_ref_from_node = nullptr;
	        }
	        if (item->_next_from_node == item_from_node)
	        {
	            item->_next_from_node = item->_iter_to_node->get_prev_from_node(item->_next_from_node);
	            item->_ref_from_node = nullptr;
	        }
	    }
}


void node::from_node_iterator::check(node_link* item_from_node, node_link* new_item_from_node)
{
	
	    for (from_node_iterator* item = _first; item != nullptr ; item = item->_next)
	    {
	        if (item->_ref_from_node == item_from_node)
	        {
	            item->_ref_from_node =             item->_prev_from_node =             item->_next_from_node = new_item_from_node;
	        }
	        if (item->_prev_from_node == item_from_node)
	        {
	            item->_prev_from_node = new_item_from_node;
	            item->_ref_from_node = nullptr;
	        }
	        if (item->_next_from_node == item_from_node)
	        {
	            item->_next_from_node = new_item_from_node;
	            item->_ref_from_node = nullptr;
	        }
	    }
}
node::from_node_iterator::~from_node_iterator()
{
	
	    if (_next)
	    {
	        _next->_prev = _prev;
	    }
	    else
	    {
	        _last = _prev;
	    }
	 
	    if (_prev)
	    {
	        _prev->_next = _next;
	    }
	    else
	    {
	        _first = _next;
	    }
}

// {user.after.class.from_node_iterator.begin}
// {user.after.class.from_node_iterator.end}

// {user.before.class.to_node_iterator.begin}
// {user.before.class.to_node_iterator.end}

node::to_node_iterator::to_node_iterator(const node* iter_from_node, bool(node_link::*method)() const, node_link* ref_to_node)
:	_ref_to_node(nullptr)
,	_prev_to_node(nullptr)
,	_next_to_node(nullptr)
,	_iter_from_node(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    assert(iter_from_node);
	 
	    _iter_from_node = iter_from_node;
	    _ref_to_node = _prev_to_node = _next_to_node = ref_to_node;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if ( _last != nullptr )
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
node::to_node_iterator::to_node_iterator(const node& iter_from_node, bool(node_link::*method)() const, node_link* ref_to_node)
:	_ref_to_node(nullptr)
,	_prev_to_node(nullptr)
,	_next_to_node(nullptr)
,	_iter_from_node(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	        
	    assert(&iter_from_node);
	 
	    _iter_from_node = &iter_from_node;
	    _ref_to_node = _prev_to_node = _next_to_node = ref_to_node;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
node::to_node_iterator::to_node_iterator(const node::to_node_iterator& iter, bool(node_link::*method)() const)
:	_ref_to_node(nullptr)
,	_prev_to_node(nullptr)
,	_next_to_node(nullptr)
,	_iter_from_node(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    _iter_from_node = iter._iter_from_node;
	    _ref_to_node = iter._ref_to_node;
	    _prev_to_node = iter._prev_to_node;
	    _next_to_node = iter._next_to_node;
	    _prev = (to_node_iterator*)0;
	    _next = (to_node_iterator*)0;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}


void node::to_node_iterator::check(node_link* item_to_node)
{
	
	    for (to_node_iterator* item = _first; item; item = item->_next)
	    {
	        if (item->_prev_to_node == item_to_node)
	        {
	            item->_prev_to_node = item->_iter_from_node->get_next_to_node(item->_prev_to_node);
	            item->_ref_to_node = nullptr;
	        }
	        if (item->_next_to_node == item_to_node)
	        {
	            item->_next_to_node = item->_iter_from_node->get_prev_to_node(item->_next_to_node);
	            item->_ref_to_node = nullptr;
	        }
	    }
}


void node::to_node_iterator::check(node_link* item_to_node, node_link* new_item_to_node)
{
	
	    for (to_node_iterator* item = _first; item != nullptr ; item = item->_next)
	    {
	        if (item->_ref_to_node == item_to_node)
	        {
	            item->_ref_to_node =             item->_prev_to_node =             item->_next_to_node = new_item_to_node;
	        }
	        if (item->_prev_to_node == item_to_node)
	        {
	            item->_prev_to_node = new_item_to_node;
	            item->_ref_to_node = nullptr;
	        }
	        if (item->_next_to_node == item_to_node)
	        {
	            item->_next_to_node = new_item_to_node;
	            item->_ref_to_node = nullptr;
	        }
	    }
}
node::to_node_iterator::~to_node_iterator()
{
	
	    if (_next)
	    {
	        _next->_prev = _prev;
	    }
	    else
	    {
	        _last = _prev;
	    }
	 
	    if (_prev)
	    {
	        _prev->_next = _next;
	    }
	    else
	    {
	        _first = _next;
	    }
}

// {user.after.class.to_node_iterator.begin}
// {user.after.class.to_node_iterator.end}

// {user.before.class.super_node_iterator.begin}
// {user.before.class.super_node_iterator.end}

node::super_node_iterator::super_node_iterator(const node* iter_node, bool(super_node::*method)() const, super_node* ref_super_node)
:	_ref_super_node(nullptr)
,	_prev_super_node(nullptr)
,	_next_super_node(nullptr)
,	_iter_node(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    assert(iter_node);
	 
	    _iter_node = iter_node;
	    _ref_super_node = _prev_super_node = _next_super_node = ref_super_node;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if ( _last != nullptr )
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
node::super_node_iterator::super_node_iterator(const node& iter_node, bool(super_node::*method)() const, super_node* ref_super_node)
:	_ref_super_node(nullptr)
,	_prev_super_node(nullptr)
,	_next_super_node(nullptr)
,	_iter_node(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	        
	    assert(&iter_node);
	 
	    _iter_node = &iter_node;
	    _ref_super_node = _prev_super_node = _next_super_node = ref_super_node;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
node::super_node_iterator::super_node_iterator(const node::super_node_iterator& iter, bool(super_node::*method)() const)
:	_ref_super_node(nullptr)
,	_prev_super_node(nullptr)
,	_next_super_node(nullptr)
,	_iter_node(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    _iter_node = iter._iter_node;
	    _ref_super_node = iter._ref_super_node;
	    _prev_super_node = iter._prev_super_node;
	    _next_super_node = iter._next_super_node;
	    _prev = (super_node_iterator*)0;
	    _next = (super_node_iterator*)0;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}


void node::super_node_iterator::check(super_node* item_super_node)
{
	
	    for (super_node_iterator* item = _first; item; item = item->_next)
	    {
	        if (item->_prev_super_node == item_super_node)
	        {
	            item->_prev_super_node = item->_iter_node->get_next_super_node(item->_prev_super_node);
	            item->_ref_super_node = nullptr;
	        }
	        if (item->_next_super_node == item_super_node)
	        {
	            item->_next_super_node = item->_iter_node->get_prev_super_node(item->_next_super_node);
	            item->_ref_super_node = nullptr;
	        }
	    }
}


void node::super_node_iterator::check(super_node* item_super_node, super_node* new_item_super_node)
{
	
	    for (super_node_iterator* item = _first; item != nullptr ; item = item->_next)
	    {
	        if (item->_ref_super_node == item_super_node)
	        {
	            item->_ref_super_node =             item->_prev_super_node =             item->_next_super_node = new_item_super_node;
	        }
	        if (item->_prev_super_node == item_super_node)
	        {
	            item->_prev_super_node = new_item_super_node;
	            item->_ref_super_node = nullptr;
	        }
	        if (item->_next_super_node == item_super_node)
	        {
	            item->_next_super_node = new_item_super_node;
	            item->_ref_super_node = nullptr;
	        }
	    }
}
node::super_node_iterator::~super_node_iterator()
{
	
	    if (_next)
	    {
	        _next->_prev = _prev;
	    }
	    else
	    {
	        _last = _prev;
	    }
	 
	    if (_prev)
	    {
	        _prev->_next = _next;
	    }
	    else
	    {
	        _first = _next;
	    }
}

// {user.after.class.super_node_iterator.begin}
// {user.after.class.super_node_iterator.end}


// {user.after.class.node.begin}
// {user.after.class.node.end}


