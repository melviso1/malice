// {user.before.include.begin}
// {user.before.include.end}

#include "malice.h"

#include <assert.h>

// {user.before.class.super_node.begin}
// {user.before.class.super_node.end}



void super_node::__init__(node* ptr_node)
{
	//init from relations:
	
	//init to relations:
	
	    //multi_owned_passive node -> super_node
	    {
	        assert(this);
	        assert(ptr_node);
	        ptr_node->_count_super_node++;
	    
	        _ref_node = ptr_node;
	    
	        if (ptr_node->_last_super_node)
	        {
	            _next_node = nullptr;
	            _prev_node = ptr_node->_last_super_node;
	            _prev_node->_next_node = this;
	            ptr_node->_last_super_node = this;
	        }
	        else
	        {
	            _prev_node = nullptr;
	            _next_node = nullptr;
	            ptr_node->_first_super_node = ptr_node->_last_super_node = this;
	        }
	    }
}


void super_node::__exit__()
{
	//init from relations:
	
	//init to relations:
	
	    assert(this);
	    if (_ref_node)
	    {
	        node::super_node_iterator::check(this);
	
	        _ref_node->_count_super_node--;
	
	        if (_next_node)
	        {
	            _next_node->_prev_node = _prev_node;
	        }
	        else
	        {
	            _ref_node->_last_super_node = _prev_node;
	        }
	
	        if (_prev_node)
	        {
	            _prev_node->_next_node = _next_node;
	        }
	        else
	        {
	            _ref_node->_first_super_node = _next_node;
	        }
	        _prev_node = nullptr;
	        _next_node = nullptr;
	        _ref_node = nullptr;
	    }
}
super_node::super_node(super_level* ptr_level, size_t index, size_t parent_index)
:	node(ptr_level, index)
,	_ref_node(nullptr)
,	_prev_node(nullptr)
,	_next_node(nullptr)
{
	level *parent_level = ptr_level->get_level();
	node *parent_node = parent_level->find_node(parent_index);
	__init__( parent_node );
		
}
super_node::super_node(level* ptr_level, node* ptr_node)
:	node(ptr_level)
,	_ref_node(nullptr)
,	_prev_node(nullptr)
,	_next_node(nullptr)
{
		__init__( ptr_node );
}


super_node* super_node::candidate()
{
	node* p_node = node::candidate();
	if( p_node != nullptr )
	{
		return dynamic_cast<super_node*>(p_node);
	}
	return nullptr;
}


void super_node::who()
{
	std::cout << " supernodo " << this ;
}
super_node::~super_node()
{
		__exit__();
}

// {user.after.class.super_node.begin}
// {user.after.class.super_node.end}


