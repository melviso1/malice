// {user.before.include.begin}
// {user.before.include.end}

#include "malice.h"

#include <assert.h>

// {user.before.class.super_level.begin}
// {user.before.class.super_level.end}



void super_level::__init__(level* ptr_level)
{
	//init from relations:
	
	//init to relations:
	
	    //single_owned_passive level -> super_level
	    assert(this);
	    assert(ptr_level);
	    assert(ptr_level->_ref_super_level == nullptr);
	
	    _ref_level = ptr_level;
	    ptr_level->_ref_super_level = this;
}


void super_level::__exit__()
{
	//init from relations:
	
	//init to relations:
	
	    assert(this);
	    if (_ref_level)
	    {
	        assert(_ref_level->_ref_super_level == this);
	        _ref_level->_ref_super_level = nullptr;
	        _ref_level = nullptr;
	    }
}
super_level::super_level(level* ptr_level, std::fstream& f)
:	level(f, true)
,	_ref_level(nullptr)
{
	__init__( ptr_level );
	load_nodes(f);
	load_nodes_relations(f);
	load_active_node(f);
	load_super_level(f);
}
super_level::super_level(level* ptr_level)
:	level()
,	_ref_level(nullptr)
{
		__init__( ptr_level );
}


super_node* super_level::get_active_node()
{
	node* p_node = level::get_active_node();
	if( p_node != nullptr )
	{
		return dynamic_cast<super_node*>(p_node);
	}
	return nullptr;
}


/**
* \details Load the symbols with their indexes. 
**/
void super_level::load_nodes(std::fstream& f)
{
	size_t node_count;
	f >> node_count;
	while(node_count--)
	{
		size_t node_index;
		size_t parent_node_index;
		f >> node_index;
		f >> parent_node_index;
		new super_node(this, node_index, parent_node_index);
	}
}


void super_level::save_nodes(std::fstream& f)
{
	f << get_node_count();
	super_node *p_super_node;
	level::node_iterator iter_node = node_iterator(this);
	while(++iter_node)
	{
		p_super_node = dynamic_cast<super_node*>(iter_node.get());
		f << p_super_node->get_index();
		f << p_super_node->get_node()->get_index();
	}
}


/**
* \details Return the expected candidate for the active node (if the node is not active, return nullptr) or a nullptr value if can't be determined.
**/
super_node* super_level::candidate()
{
	node *p_node = level::candidate();
	if( p_node != nullptr )
	{
		return dynamic_cast<super_node*>(p_node);
	}
	return nullptr;
}
super_level::~super_level()
{
		__exit__();
}

// {user.after.class.super_level.begin}
// {user.after.class.super_level.end}


