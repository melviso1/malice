// {user.before.include.begin}
// {user.before.include.end}

#include "malice.h"

#include <assert.h>

// {user.before.class.interface_level.begin}
// {user.before.class.interface_level.end}

symbol* interface_level::_first_symbol = nullptr;
size_t interface_level::_count_symbol = 0;
interface_level::symbol_iterator* interface_level::symbol_iterator::_first = nullptr;
interface_level::symbol_iterator* interface_level::symbol_iterator::_last = nullptr;


void interface_level::__exit__()
{
	//init from relations:
	
	    { 
	        for (symbol* item = get_first_symbol(); item; item = get_first_symbol())
	        {
	            delete item; 
	        }
	    }
	
	//init to relations:
}
interface_level::interface_level()
:	level()
{
	create_symbol_table();
}


void interface_level::create_symbol_table()
{
	size_t count = 0;
	while ( count < 256 )
	{
		new symbol(this, count ++);
	}
}


/**
* \details Get next symbol
**/
bool interface_level::get(char& ch)
{
	node *p_node = activate_candidate();
	if( p_node == nullptr )
	{
		ch='?';
		return false;
	}
	symbol *p_symbol = dynamic_cast<symbol*>(p_node);
	ch = char(p_symbol->get_identifier());
	#if defined(DEBUG)
	if(find_symbol(size_t(ch)) != p_symbol)
	{
		std::cout << "Error en conversion de simbolos." << std::endl;
	}
	#endif
	return true;
}


bool interface_level::set(char ch)
{
	size_t value = size_t(ch);
	symbol *p_symbol = find_symbol(value);
	if( p_symbol == nullptr )
	{
		std::cout << "Error fatal!!. No se puede encontrar el simbolo para el caracter '" <<ch <<"'"<<std::endl;
		return false;
	}
	p_symbol->activate();
	return true;
}


void interface_level::save_nodes(std::fstream& f)
{
	f << get_symbol_count();
	symbol_iterator iter_symbol = symbol_iterator(this);
	while(++iter_symbol)
	{
		f << iter_symbol->get_identifier();
		f << iter_symbol->get_index();
	}
}


/**
* \details Load the symbols with their indexes. 
**/
void interface_level::load_nodes(std::fstream& f)
{
	size_t  symbol_count;
	f >> symbol_count;
	while(symbol_count--)
	{
		size_t identifier;
		size_t index;
		f >> identifier;
		f >> index;
		new symbol(this, index, identifier);
	}
}


void interface_level::load(std::fstream& f)
{
	level::load(f);
	f >> level::_index_level;
	f >> node::_index_node;
}


void interface_level::save(std::fstream& f)
{
	level::save(f);
	f << level::_index_level;
	f << node::_index_node;
}


void interface_level::add_symbol(symbol* item)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_interface_level == nullptr);
	
	    _count_symbol++;
	
	    item->_ref_interface_level = this;
	
	    if (_first_symbol)
	    {
	        symbol* current = _first_symbol;
	        unsigned long bit = 0x1;
	        while (1)
	        {
	            assert(current->get_identifier() != item->get_identifier());
	
	            if ((current->get_identifier() & bit) == (item->get_identifier() & bit))
	            {
	                if (current->_left_interface_level)
	                {
	                    current = current->_left_interface_level;
	                }
	                else
	                {
	                    current->_left_interface_level = item;
	                    item->_parent_interface_level = current;
	                    break;
	                }
	            }
	            else
	            {
	                if (current->_right_interface_level)
	                {
	                    current = current->_right_interface_level;
	                }
	                else
	                {
	                    current->_right_interface_level = item;
	                    item->_parent_interface_level = current;
	                    break;
	                }
	            }
	
	            bit <<= 1;
	        }
	    }
	    else
	    {
	        _first_symbol = item;
	    }
}


void interface_level::remove_symbol(symbol* item)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_interface_level == this);
	
	    interface_level::symbol_iterator::check(item);
	
	    _count_symbol--;
	
	    symbol* replacement = 0;
	    symbol* move = 0;
	    if (item->_left_interface_level)
	    {
	        replacement = item->_left_interface_level;
	        replacement->_parent_interface_level = item->_parent_interface_level;
	        move = item->_right_interface_level;
	    }
	    else if (item->_right_interface_level)
	    {
	        replacement = item->_right_interface_level;
	        replacement->_parent_interface_level = item->_parent_interface_level;
	    }
	
	    symbol* parent = item->_parent_interface_level;
	    if (parent)
	    {
	        if (parent->_left_interface_level == item)
	        {
	            parent->_left_interface_level = replacement;
	        }
	        else
	        {
	            parent->_right_interface_level = replacement;
	        }
	    }
	    else
	    {
	        _first_symbol = replacement;
	    }
	
	    if (replacement)
	    {
	        while (1)
	        {
	            symbol* tmp = replacement->_right_interface_level;
	            replacement->_right_interface_level = move;
	            if (move)
	            {
	                move->_parent_interface_level = replacement;
	            }
	            
	            if (!replacement->_left_interface_level)
	            {
	                if (tmp)
	                {
	                    replacement->_left_interface_level = tmp;
	                    tmp = 0;
	                }
	                else
	                {
	                    break;
	                }
	            }
	            move = tmp;
	            replacement = replacement->_left_interface_level;
	        }
	    }
	
	    item->_ref_interface_level = (interface_level*)0;
	    item->_parent_interface_level = (symbol*)0;
	    item->_left_interface_level = (symbol*)0;
	    item->_right_interface_level = (symbol*)0;
}


void interface_level::delete_all_symbol()
{
	
	    assert(this);
	
	    for (symbol* item = get_first_symbol(); item; item = get_first_symbol())
	          delete item;
}


void interface_level::remove_symbol(symbol* item, symbol* new_item)
{
	
	    assert(this);
	
	    assert(item);
	    assert(item->_ref_interface_level == this);
	
	    assert(new_item);
	    assert(new_item->_ref_interface_level == (interface_level*)0);
	
	    if (item->get_identifier() == new_item->get_identifier())
	    {
	        interface_level::symbol_iterator::check(item, new_item);
	        if (_first_symbol == item)
	        {
	            _first_symbol = new_item;
	        }
	        if (item->_parent_interface_level)
	        {
	            if (item->_parent_interface_level->_left_interface_level == item)
	            {
	                item->_parent_interface_level->_left_interface_level = new_item;
	            }
	            else if (item->_parent_interface_level->_right_interface_level == item)
	            {
	                item->_parent_interface_level->_right_interface_level = new_item;
	            }
	        }
	        new_item->_ref_interface_level = this;
	        new_item->_parent_interface_level = item->_parent_interface_level;
	        new_item->_left_interface_level = item->_left_interface_level;
	        new_item->_right_interface_level = item->_right_interface_level;
	        item->_ref_interface_level = (interface_level*)0;
	        item->_parent_interface_level = (symbol*)0;
	        item->_left_interface_level = (symbol*)0;
	        item->_right_interface_level = (symbol*)0;
	    }
	    else
	    {
	        interface_level::symbol_iterator::check(item);
	        remove_symbol(item);
	        add_symbol(new_item);
	    }
}


symbol* interface_level::get_first_symbol() const
{
	
	    assert(this);
	    return _first_symbol;
}


symbol* interface_level::get_last_symbol() const
{
	
	    assert(this);
	
	    symbol* result = _first_symbol;
	    while (result)
	    {
	        while (result->_right_interface_level)
	        {
	            result = result->_right_interface_level;
	        }
	
	        if (result->_left_interface_level)
	        {
	            result = result->_left_interface_level;
	        }
	        else
	        {
	            break;
	        }
	    }
	
	    return result;
}


symbol* interface_level::get_next_symbol(symbol* pos) const
{
	
	    assert(this);
	
	    symbol* result = 0;
	    if (pos == (symbol*)0)
	        result = _first_symbol;
	    else
	    {
	        assert(pos->_ref_interface_level == this);
	
	        if (pos->_left_interface_level)
	        {
	            result = pos->_left_interface_level;
	        }
	        else
	        {
	            if (pos->_right_interface_level)
	            {
	                result = pos->_right_interface_level;
	            }
	            else
	            {
	                symbol* parent = pos->_parent_interface_level;
	                while (parent && (parent->_right_interface_level == 0 || parent->_right_interface_level == pos))
	                {
	                    pos = parent;
	                    parent = parent->_parent_interface_level;
	                }
	
	                if (parent)
	                {
	                    result = parent->_right_interface_level;
	                }
	            }
	        }
	    }
	
	    return result;
}


symbol* interface_level::get_prev_symbol(symbol* pos) const
{
	
	    assert(this);
	
	    symbol* result = 0;
	    if (pos == (symbol*)0)
	    {
	        result = get_last_symbol();
	    }
	    else
	    {
	        assert(pos->_ref_interface_level == this);
	
	        if (pos->_parent_interface_level)
	        {
	            if (pos->_parent_interface_level->_left_interface_level == pos || pos->_parent_interface_level->_left_interface_level == 0)
	            {
	                result = pos->_parent_interface_level;
	            }
	            else /* Right branche and valid left branche */
	            {
	                result = pos->_parent_interface_level->_left_interface_level;
	                while (1)
	                {
	                    while (result->_right_interface_level)
	                    {
	                        result = result->_right_interface_level;
	                    }
	
	                    if (result->_left_interface_level)
	                    {
	                        result = result->_left_interface_level;
	                    }
	                    else
	                    {
	                        break;
	                    }
	                }
	            }
	        }
	    }
	
	    return result;
}


size_t interface_level::get_symbol_count()
{
	
	    assert(this);
	    return _count_symbol;
}
interface_level::~interface_level()
{
		__exit__();
}
// {user.before.class.symbol_iterator.begin}
// {user.before.class.symbol_iterator.end}

interface_level::symbol_iterator::symbol_iterator(const interface_level* iter_interface_level, bool(symbol::*method)() const, symbol* ref_symbol)
:	_ref_symbol(nullptr)
,	_prev_symbol(nullptr)
,	_next_symbol(nullptr)
,	_iter_interface_level(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    assert(iter_interface_level);
	 
	    _iter_interface_level = iter_interface_level;
	    _ref_symbol = _prev_symbol = _next_symbol = ref_symbol;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if ( _last != nullptr )
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
interface_level::symbol_iterator::symbol_iterator(const interface_level& iter_interface_level, bool(symbol::*method)() const, symbol* ref_symbol)
:	_ref_symbol(nullptr)
,	_prev_symbol(nullptr)
,	_next_symbol(nullptr)
,	_iter_interface_level(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	        
	    assert(&iter_interface_level);
	 
	    _iter_interface_level = &iter_interface_level;
	    _ref_symbol = _prev_symbol = _next_symbol = ref_symbol;
	    _prev = nullptr;
	    _next = nullptr;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}
interface_level::symbol_iterator::symbol_iterator(const interface_level::symbol_iterator& iter, bool(symbol::*method)() const)
:	_ref_symbol(nullptr)
,	_prev_symbol(nullptr)
,	_next_symbol(nullptr)
,	_iter_interface_level(nullptr)
,	_prev(nullptr)
,	_next(nullptr)
,	_method(nullptr)
{
	
	    _iter_interface_level = iter._iter_interface_level;
	    _ref_symbol = iter._ref_symbol;
	    _prev_symbol = iter._prev_symbol;
	    _next_symbol = iter._next_symbol;
	    _prev = (symbol_iterator*)0;
	    _next = (symbol_iterator*)0;
	    _method = method;
	    if (_last)
	    {
	        _last->_next = this;
	        _prev = _last;
	        _last = this;
	    }
	    else
	    {
	        _first = _last = this;
	    }
}


void interface_level::symbol_iterator::check(symbol* item_symbol)
{
	
	    for (symbol_iterator* item = _first; item; item = item->_next)
	    {
	        if (item->_prev_symbol == item_symbol)
	        {
	            item->_prev_symbol = item->_iter_interface_level->get_next_symbol(item->_prev_symbol);
	            item->_ref_symbol = nullptr;
	        }
	        if (item->_next_symbol == item_symbol)
	        {
	            item->_next_symbol = item->_iter_interface_level->get_prev_symbol(item->_next_symbol);
	            item->_ref_symbol = nullptr;
	        }
	    }
}


void interface_level::symbol_iterator::check(symbol* item_symbol, symbol* new_item_symbol)
{
	
	    for (symbol_iterator* item = _first; item != nullptr ; item = item->_next)
	    {
	        if (item->_ref_symbol == item_symbol)
	        {
	            item->_ref_symbol =             item->_prev_symbol =             item->_next_symbol = new_item_symbol;
	        }
	        if (item->_prev_symbol == item_symbol)
	        {
	            item->_prev_symbol = new_item_symbol;
	            item->_ref_symbol = nullptr;
	        }
	        if (item->_next_symbol == item_symbol)
	        {
	            item->_next_symbol = new_item_symbol;
	            item->_ref_symbol = nullptr;
	        }
	    }
}
interface_level::symbol_iterator::~symbol_iterator()
{
	
	    if (_next)
	    {
	        _next->_prev = _prev;
	    }
	    else
	    {
	        _last = _prev;
	    }
	 
	    if (_prev)
	    {
	        _prev->_next = _next;
	    }
	    else
	    {
	        _first = _next;
	    }
}

// {user.after.class.symbol_iterator.begin}
// {user.after.class.symbol_iterator.end}


// {user.after.class.interface_level.begin}
// {user.after.class.interface_level.end}


