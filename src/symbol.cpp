// {user.before.include.begin}
// {user.before.include.end}

#include "malice.h"

#include <assert.h>

// {user.before.class.symbol.begin}
// {user.before.class.symbol.end}



void symbol::__init__(interface_level* ptr_interface_level)
{
	//init from relations:
	
	//init to relations:
	
	    //uniquevaluetree_owned_passive interface_level -> symbol
	    assert(this);
	    _ref_interface_level = nullptr;
	    _parent_interface_level = nullptr;
	    _left_interface_level = nullptr;
	    _right_interface_level = nullptr;
	    assert(ptr_interface_level);
	    ptr_interface_level->add_symbol(this);
}


void symbol::__exit__()
{
	//init from relations:
	
	//init to relations:
	
	    if (_ref_interface_level)
	    {
	        _ref_interface_level->remove_symbol(this);
	    }
}
symbol::symbol(interface_level* ptr_interface_level, size_t index, size_t identifier)
:	node(ptr_interface_level, index)
,	_identifier(identifier)
,	_ref_interface_level(nullptr)
,	_parent_interface_level(nullptr)
,	_left_interface_level(nullptr)
,	_right_interface_level(nullptr)
{
		__init__( ptr_interface_level );
}
symbol::symbol(interface_level* ptr_interface_level, size_t identifier)
:	node(ptr_interface_level)
,	_identifier(identifier)
,	_ref_interface_level(nullptr)
,	_parent_interface_level(nullptr)
,	_left_interface_level(nullptr)
,	_right_interface_level(nullptr)
{
		__init__( ptr_interface_level );
}


void symbol::who()
{
	std::cout << " nodo " << this << " correspondiente al caracter " << (char)get_identifier();
}
symbol::~symbol()
{
		__exit__();
}

// {user.after.class.symbol.begin}
// {user.after.class.symbol.end}


