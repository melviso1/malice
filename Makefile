# definitions

CC = g++
AR = ar
LINK = g++
CPPFLAGS =  -Iinclude  -O0 
SRC = src
LINKFLAGS = 

all : malice

malice: node_link.o node.o level.o super_node.o super_level.o symbol.o interface_level.o main.o
	$(CC) node_link.o node.o level.o super_node.o super_level.o symbol.o interface_level.o main.o $(LINKFLAGS) -o malice

node_link.o: src/node_link.cpp include/node_link.h
	$(CC) $(CPPFLAGS) -c src/node_link.cpp -o node_link.o

node.o: src/node.cpp include/node.h
	$(CC) $(CPPFLAGS) -c src/node.cpp -o node.o

level.o: src/level.cpp include/level.h
	$(CC) $(CPPFLAGS) -c src/level.cpp -o level.o

super_node.o: src/super_node.cpp include/super_node.h
	$(CC) $(CPPFLAGS) -c src/super_node.cpp -o super_node.o

super_level.o: src/super_level.cpp include/super_level.h
	$(CC) $(CPPFLAGS) -c src/super_level.cpp -o super_level.o

symbol.o: src/symbol.cpp include/symbol.h
	$(CC) $(CPPFLAGS) -c src/symbol.cpp -o symbol.o

interface_level.o: src/interface_level.cpp include/interface_level.h
	$(CC) $(CPPFLAGS) -c src/interface_level.cpp -o interface_level.o

main.o: src/main.cpp include/main.h
	$(CC) $(CPPFLAGS) -c src/main.cpp -o main.o

clean:
	rm -f *.o malice

.PHONY: clean 

