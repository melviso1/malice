#if !defined(LEVEL_H_INCLUDED)
#define LEVEL_H_INCLUDED


// {user.before.class level.begin}
#include <fstream>
// {user.before.class level.end}

/**
* This class represents a layer of nodes.
**/
class level
{

	// {user.inside.first.class level.begin}
	// {user.inside.first.class level.end}

	friend class interface_level;
	friend class node;
	friend class super_level;
	public:
	// {user.before.class node_iterator.begin}
	// {user.before.class node_iterator.end}

	class node_iterator
	{

		// {user.inside.first.class node_iterator.begin}
		// {user.inside.first.class node_iterator.end}

		node* _ref_node ;
		node* _prev_node ;
		node* _next_node ;
		const level* _iter_level ;
		level::node_iterator* _prev ;
		level::node_iterator* _next ;
		bool(node::*_method)() const ;
		static level::node_iterator* _first ;
		static level::node_iterator* _last ;
		public:
		node_iterator(const level* iter_level, bool(node::*method)() const=nullptr, node* ref_node=nullptr);
		node_iterator(const level& iter_level, bool(node::*method)() const=nullptr, node* ref_node=nullptr);
		node_iterator(const level::node_iterator& iter, bool(node::*method)() const=nullptr);
		static void check(node* item_node);
		static void check(node* item_node, node* new_item_node);
		inline level::node_iterator& operator =(const level::node_iterator& iter);
		inline node* operator ++();
		inline node* operator --();
		inline  operator node*();
		inline node* operator ->();
		inline node* get();
		inline void reset();
		inline bool is_first() const;
		inline bool is_last() const;
		virtual ~node_iterator();
		// {user.inside.last.class node_iterator.begin}
		// {user.inside.last.class node_iterator.end}
	};

	// {user.after.class node_iterator.begin}
	// {user.after.class node_iterator.end}

	node* find_node(size_t index);
	void set_active_node(node* p_node);
	private:
	node* _ref_active_node ;
	public:
	void add_active_node(node* item);
	void remove_active_node(node* item);
	void replace_active_node(node* item, node* new_item);
	void move_active_node(node* item);
	inline node* get_active_node();
	private:
	node* _first_node ;
	node* _last_node ;
	int _count_node ;
	protected:
	void add_node_first(node* item);
	void add_node_last(node* item);
	void add_node_after(node* item, node* pos);
	void add_node_before(node* item, node* pos);
	void remove_node(node* item);
	void replace_node(node* item, node* new_item);
	public:
	void delete_all_node();
	node* get_first_node() const;
	node* get_last_node() const;
	node* get_next_node(node* pos) const;
	node* get_prev_node(node* pos) const;
	int get_node_count();
	void move_node_first(node* item);
	void move_node_last(node* item);
	void move_node_after(node* item, node* pos);
	void move_node_before(node* item, node* pos);
	void sort_node(int (*compare)(node*,node*));
	private:
	super_level* _ref_super_level ;
	protected:
	void add_super_level(super_level* item);
	void remove_super_level(super_level* item);
	void replace_super_level(super_level* item, super_level* new_item);
	public:
	void move_super_level(super_level* item);
	inline super_level* get_super_level();
	protected:
	static size_t _index_level ;
	public:
	size_t _index ;
	inline const size_t get_index() const;
	void save_super_level(std::fstream& f);
	void save_active_node(std::fstream& f);
	void save_nodes_relations(std::fstream& f);
	virtual void save_nodes(std::fstream& f);
	void save(std::fstream& f);
	void load_super_level(std::fstream& f);
	void load_active_node(std::fstream& f);
	void cleanup();
	void load_nodes_relations(std::fstream& f);
	virtual void load_nodes(std::fstream& f);
	void load(std::fstream& f);
	level(std::fstream& f, bool override=false);
	node* candidate();
	super_node* super_candidate();
	node* activate_candidate();
	void __exit__();
	virtual ~level();
	level();
	// {user.inside.last.class level.begin}
	// {user.inside.last.class level.end}
};

// {user.after.class level.begin}
// {user.after.class level.end}

#endif //LEVEL_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_LEVEL)
#define INCLUDE_INLINES_LEVEL



inline level::node_iterator& level::node_iterator::operator =(const level::node_iterator& iter)
{
	
	    _iter_level = iter._iter_level;
	    _ref_node    = iter._ref_node;
	    _prev_node   = iter._prev_node;
	    _next_node   = iter._next_node;
	    _method = iter._method;
	    return *this;
}


inline node* level::node_iterator::operator ++()
{
	
	    _next_node = _iter_level->get_next_node(_next_node);
	    if ( _method != nullptr )
	    {
	        while (_next_node != nullptr && !(_next_node->*_method)())
	        {
	            _next_node = _iter_level->get_next_node(_next_node);
	        }
	    }
	    _ref_node = _prev_node = _next_node;
	    return _ref_node;
}


inline node* level::node_iterator::operator --()
{
	
	    _prev_node = _iter_level->get_prev_node(_prev_node);
	    if (_method != 0)
	    {
	        while (_prev_node && !(_prev_node->*_method)())
	        {
	            _prev_node = _iter_level->get_prev_node(_prev_node);
	        }
	    }
	    _ref_node = _next_node = _prev_node;
	    return _ref_node;
}


inline  level::node_iterator::operator node*()
{
	
	    return _ref_node;
}


inline node* level::node_iterator::operator ->()
{
	
	    return _ref_node;
}


inline node* level::node_iterator::get()
{
	
	    return _ref_node;
}


inline void level::node_iterator::reset()
{
	
	    _ref_node = _prev_node = _next_node = nullptr;
	        
}


inline bool level::node_iterator::is_first() const
{
	
	    return (_iter_level->get_first_node() == _ref_node);        
}


inline bool level::node_iterator::is_last() const
{
	
	    return (_iter_level->get_last_node() == _ref_node);
}


inline node* level::get_active_node()
{
	
	    return _ref_active_node; 
}


inline super_level* level::get_super_level()
{
	
	    return _ref_super_level; 
}


inline const size_t level::get_index() const
{
	return _index;
}

#endif //(INCLUDE_INLINES_LEVEL)
#endif //INCLUDE_INLINES


