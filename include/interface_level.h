#if !defined(INTERFACE_LEVEL_H_INCLUDED)
#define INTERFACE_LEVEL_H_INCLUDED


// {user.before.class interface_level.begin}
// {user.before.class interface_level.end}

class interface_level: public level
{

	// {user.inside.first.class interface_level.begin}
	// {user.inside.first.class interface_level.end}

	public:
	// {user.before.class symbol_iterator.begin}
	// {user.before.class symbol_iterator.end}

	class symbol_iterator
	{

		// {user.inside.first.class symbol_iterator.begin}
		// {user.inside.first.class symbol_iterator.end}

		symbol* _ref_symbol ;
		symbol* _prev_symbol ;
		symbol* _next_symbol ;
		const interface_level* _iter_interface_level ;
		interface_level::symbol_iterator* _prev ;
		interface_level::symbol_iterator* _next ;
		bool(symbol::*_method)() const ;
		static interface_level::symbol_iterator* _first ;
		static interface_level::symbol_iterator* _last ;
		public:
		virtual ~symbol_iterator();
		static void check(symbol* item_symbol);
		static void check(symbol* item_symbol, symbol* new_item_symbol);
		inline interface_level::symbol_iterator& operator =(const interface_level::symbol_iterator& iter);
		inline symbol* operator ++();
		inline symbol* operator --();
		inline  operator symbol*();
		inline symbol* operator ->();
		inline symbol* get();
		inline void reset();
		inline bool is_first() const;
		inline bool is_last() const;
		symbol_iterator(const interface_level* iter_interface_level, bool(symbol::*method)() const=nullptr, symbol* ref_symbol=nullptr);
		symbol_iterator(const interface_level& iter_interface_level, bool(symbol::*method)() const=nullptr, symbol* ref_symbol=nullptr);
		symbol_iterator(const interface_level::symbol_iterator& iter, bool(symbol::*method)() const=nullptr);
		// {user.inside.last.class symbol_iterator.begin}
		// {user.inside.last.class symbol_iterator.end}
	};

	// {user.after.class symbol_iterator.begin}
	// {user.after.class symbol_iterator.end}

	void create_symbol_table();
	bool get(char& ch);
	bool set(char ch);
	virtual void save_nodes(std::fstream& f);
	virtual void load_nodes(std::fstream& f);
	virtual void load(std::fstream& f);
	virtual void save(std::fstream& f);
	private:
	static symbol* _first_symbol ;
	static size_t _count_symbol ;
	public:
	void add_symbol(symbol* item);
	void remove_symbol(symbol* item);
	void delete_all_symbol();
	void remove_symbol(symbol* item, symbol* new_item);
	symbol* get_first_symbol() const;
	symbol* get_last_symbol() const;
	symbol* get_next_symbol(symbol* pos) const;
	symbol* get_prev_symbol(symbol* pos) const;
	size_t get_symbol_count();
	inline symbol* find_symbol(const size_t& value) const;
	void __exit__();
	virtual ~interface_level();
	interface_level();
	// {user.inside.last.class interface_level.begin}
	// {user.inside.last.class interface_level.end}
};

// {user.after.class interface_level.begin}
// {user.after.class interface_level.end}

#endif //INTERFACE_LEVEL_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_INTERFACE_LEVEL)
#define INCLUDE_INLINES_INTERFACE_LEVEL



inline interface_level::symbol_iterator& interface_level::symbol_iterator::operator =(const interface_level::symbol_iterator& iter)
{
	
	    _iter_interface_level = iter._iter_interface_level;
	    _ref_symbol    = iter._ref_symbol;
	    _prev_symbol   = iter._prev_symbol;
	    _next_symbol   = iter._next_symbol;
	    _method = iter._method;
	    return *this;
}


inline symbol* interface_level::symbol_iterator::operator ++()
{
	
	    _next_symbol = _iter_interface_level->get_next_symbol(_next_symbol);
	    if ( _method != nullptr )
	    {
	        while (_next_symbol != nullptr && !(_next_symbol->*_method)())
	        {
	            _next_symbol = _iter_interface_level->get_next_symbol(_next_symbol);
	        }
	    }
	    _ref_symbol = _prev_symbol = _next_symbol;
	    return _ref_symbol;
}


inline symbol* interface_level::symbol_iterator::operator --()
{
	
	    _prev_symbol = _iter_interface_level->get_prev_symbol(_prev_symbol);
	    if (_method != 0)
	    {
	        while (_prev_symbol && !(_prev_symbol->*_method)())
	        {
	            _prev_symbol = _iter_interface_level->get_prev_symbol(_prev_symbol);
	        }
	    }
	    _ref_symbol = _next_symbol = _prev_symbol;
	    return _ref_symbol;
}


inline  interface_level::symbol_iterator::operator symbol*()
{
	
	    return _ref_symbol;
}


inline symbol* interface_level::symbol_iterator::operator ->()
{
	
	    return _ref_symbol;
}


inline symbol* interface_level::symbol_iterator::get()
{
	
	    return _ref_symbol;
}


inline void interface_level::symbol_iterator::reset()
{
	
	    _ref_symbol = _prev_symbol = _next_symbol = nullptr;
	        
}


inline bool interface_level::symbol_iterator::is_first() const
{
	
	    return (_iter_interface_level->get_first_symbol() == _ref_symbol);        
}


inline bool interface_level::symbol_iterator::is_last() const
{
	
	    return (_iter_interface_level->get_last_symbol() == _ref_symbol);
}


inline symbol* interface_level::find_symbol(const size_t& value) const
{
	
	    symbol* result = 0;
	    if (_first_symbol)
	    {
	        symbol* item = _first_symbol;
	        unsigned long bit = 0x1;
	        while (1)
	        {
	            if (item->get_identifier() == value)
	            {
	                result = item;
	                break;
	            }
	
	            if ((item->get_identifier() & bit) == (value & bit))
	            {
	                if (item->_left_interface_level)
	                {
	                    item = item->_left_interface_level;
	                }
	                else
	                {
	                    break;
	                }
	            }
	            else
	            {
	                if (item->_right_interface_level)
	                {
	                    item = item->_right_interface_level;
	                }
	                else
	                {
	                    break;
	                }
	            }
	
	            bit <<= 1;
	        }
	    }
	    return result;
}

#endif //(INCLUDE_INLINES_INTERFACE_LEVEL)
#endif //INCLUDE_INLINES


