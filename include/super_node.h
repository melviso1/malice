#if !defined(SUPER_NODE_H_INCLUDED)
#define SUPER_NODE_H_INCLUDED


// {user.before.class super_node.begin}
// {user.before.class super_node.end}

class super_node: public node
{

	// {user.inside.first.class super_node.begin}
	// {user.inside.first.class super_node.end}

	friend class node;
	public:
	super_node(super_level* ptr_level, size_t index, size_t parent_index);
	private:
	node* _ref_node ;
	super_node* _prev_node ;
	super_node* _next_node ;
	public:
	inline node* get_node() const;
	super_node* candidate();
	virtual void who();
	void __exit__();
	void __init__(node* ptr_node);
	virtual ~super_node();
	super_node(level* ptr_level, node* ptr_node);
	// {user.inside.last.class super_node.begin}
	// {user.inside.last.class super_node.end}
};

// {user.after.class super_node.begin}
// {user.after.class super_node.end}

#endif //SUPER_NODE_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_SUPER_NODE)
#define INCLUDE_INLINES_SUPER_NODE



inline node* super_node::get_node() const
{
	return _ref_node;
}

#endif //(INCLUDE_INLINES_SUPER_NODE)
#endif //INCLUDE_INLINES


