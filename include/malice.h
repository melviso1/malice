/***

 File    :malice.h
 Created :20/09/2018 23:19:09
 Author  :

 Licensed under wxWidgets license

***/

#if !defined(MALICE_H_INCLUDED)
#define MALICE_H_INCLUDED

//standard includes
#include <cstddef>
#include <cassert>

/**
context OVERLOAD_LEVEL:
**/

#define OVERLOAD_LEVEL 3

//forward references
class node_link;
class node;
class level;
class super_node;
class super_level;
class symbol;
class interface_level;

#include "node_link.h"
#include "node.h"
#include "level.h"
#include "super_node.h"
#include "super_level.h"
#include "symbol.h"
#include "interface_level.h"

#define INCLUDE_INLINES
#include "node_link.h"
#include "node.h"
#include "level.h"
#include "super_node.h"
#include "super_level.h"
#include "symbol.h"
#include "interface_level.h"

#include "main.h"

#endif //MALICE_H_INCLUDED

