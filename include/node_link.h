#if !defined(NODE_LINK_H_INCLUDED)
#define NODE_LINK_H_INCLUDED


// {user.before.class node_link.begin}
// {user.before.class node_link.end}

class node_link
{

	// {user.inside.first.class node_link.begin}
	// {user.inside.first.class node_link.end}

	friend class node;
	node* _ref_to_node ;
	node_link* _prev_to_node ;
	node_link* _next_to_node ;
	public:
	inline node* get_to_node() const;
	private:
	node* _ref_from_node ;
	node_link* _prev_from_node ;
	node_link* _next_from_node ;
	public:
	inline node* get_from_node() const;
	void __exit__();
	void __init__(node* ptr_from_node, node* ptr_to_node);
	virtual ~node_link();
	node_link(node* ptr_from_node, node* ptr_to_node);
	// {user.inside.last.class node_link.begin}
	// {user.inside.last.class node_link.end}
};

// {user.after.class node_link.begin}
// {user.after.class node_link.end}

#endif //NODE_LINK_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_NODE_LINK)
#define INCLUDE_INLINES_NODE_LINK



inline node* node_link::get_to_node() const
{
	return _ref_to_node;
}


inline node* node_link::get_from_node() const
{
	return _ref_from_node;
}

#endif //(INCLUDE_INLINES_NODE_LINK)
#endif //INCLUDE_INLINES


