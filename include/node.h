#if !defined(NODE_H_INCLUDED)
#define NODE_H_INCLUDED


// {user.before.class node.begin}
#include <fstream>
// {user.before.class node.end}

class node
{

	// {user.inside.first.class node.begin}
	// {user.inside.first.class node.end}

	friend class interface_level;
	friend class node_link;
	friend class super_node;
	friend class level;
	public:
	// {user.before.class from_node_iterator.begin}
	// {user.before.class from_node_iterator.end}

	class from_node_iterator
	{

		// {user.inside.first.class from_node_iterator.begin}
		// {user.inside.first.class from_node_iterator.end}

		public:
		from_node_iterator(const node* iter_to_node, bool(node_link::*method)() const=nullptr, node_link* ref_from_node=nullptr);
		from_node_iterator(const node& iter_to_node, bool(node_link::*method)() const=nullptr, node_link* ref_from_node=nullptr);
		from_node_iterator(const node::from_node_iterator& iter, bool(node_link::*method)() const=nullptr);
		private:
		node_link* _ref_from_node ;
		node_link* _prev_from_node ;
		node_link* _next_from_node ;
		const node* _iter_to_node ;
		node::from_node_iterator* _prev ;
		node::from_node_iterator* _next ;
		bool(node_link::*_method)() const ;
		static node::from_node_iterator* _first ;
		static node::from_node_iterator* _last ;
		public:
		static void check(node_link* item_from_node);
		static void check(node_link* item_from_node, node_link* new_item_from_node);
		inline node::from_node_iterator& operator =(const node::from_node_iterator& iter);
		inline node_link* operator ++();
		inline node_link* operator --();
		inline  operator node_link*();
		inline node_link* operator ->();
		inline node_link* get();
		inline void reset();
		inline bool is_first() const;
		inline bool is_last() const;
		virtual ~from_node_iterator();
		// {user.inside.last.class from_node_iterator.begin}
		// {user.inside.last.class from_node_iterator.end}
	};

	// {user.after.class from_node_iterator.begin}
	// {user.after.class from_node_iterator.end}

	// {user.before.class to_node_iterator.begin}
	// {user.before.class to_node_iterator.end}

	class to_node_iterator
	{

		// {user.inside.first.class to_node_iterator.begin}
		// {user.inside.first.class to_node_iterator.end}

		public:
		to_node_iterator(const node* iter_from_node, bool(node_link::*method)() const=nullptr, node_link* ref_to_node=nullptr);
		to_node_iterator(const node& iter_from_node, bool(node_link::*method)() const=nullptr, node_link* ref_to_node=nullptr);
		to_node_iterator(const node::to_node_iterator& iter, bool(node_link::*method)() const=nullptr);
		private:
		node_link* _ref_to_node ;
		node_link* _prev_to_node ;
		node_link* _next_to_node ;
		const node* _iter_from_node ;
		node::to_node_iterator* _prev ;
		node::to_node_iterator* _next ;
		bool(node_link::*_method)() const ;
		static node::to_node_iterator* _first ;
		static node::to_node_iterator* _last ;
		public:
		static void check(node_link* item_to_node);
		static void check(node_link* item_to_node, node_link* new_item_to_node);
		inline node::to_node_iterator& operator =(const node::to_node_iterator& iter);
		inline node_link* operator ++();
		inline node_link* operator --();
		inline  operator node_link*();
		inline node_link* operator ->();
		inline node_link* get();
		inline void reset();
		inline bool is_first() const;
		inline bool is_last() const;
		virtual ~to_node_iterator();
		// {user.inside.last.class to_node_iterator.begin}
		// {user.inside.last.class to_node_iterator.end}
	};

	// {user.after.class to_node_iterator.begin}
	// {user.after.class to_node_iterator.end}

	// {user.before.class super_node_iterator.begin}
	// {user.before.class super_node_iterator.end}

	class super_node_iterator
	{

		// {user.inside.first.class super_node_iterator.begin}
		// {user.inside.first.class super_node_iterator.end}

		public:
		super_node_iterator(const node* iter_node, bool(super_node::*method)() const=nullptr, super_node* ref_super_node=nullptr);
		super_node_iterator(const node& iter_node, bool(super_node::*method)() const=nullptr, super_node* ref_super_node=nullptr);
		super_node_iterator(const node::super_node_iterator& iter, bool(super_node::*method)() const=nullptr);
		private:
		super_node* _ref_super_node ;
		super_node* _prev_super_node ;
		super_node* _next_super_node ;
		const node* _iter_node ;
		node::super_node_iterator* _prev ;
		node::super_node_iterator* _next ;
		bool(super_node::*_method)() const ;
		static node::super_node_iterator* _first ;
		static node::super_node_iterator* _last ;
		public:
		static void check(super_node* item_super_node);
		static void check(super_node* item_super_node, super_node* new_item_super_node);
		inline node::super_node_iterator& operator =(const node::super_node_iterator& iter);
		inline super_node* operator ++();
		inline super_node* operator --();
		inline  operator super_node*();
		inline super_node* operator ->();
		inline super_node* get();
		inline void reset();
		inline bool is_first() const;
		inline bool is_last() const;
		virtual ~super_node_iterator();
		// {user.inside.last.class super_node_iterator.begin}
		// {user.inside.last.class super_node_iterator.end}
	};

	// {user.after.class super_node_iterator.begin}
	// {user.after.class super_node_iterator.end}

	virtual ~node();
	bool active() const;
	void __exit__();
	void __init__(level* ptr_level);
	private:
	node_link* _first_from_node ;
	node_link* _last_from_node ;
	int _count_from_node ;
	protected:
	void add_from_node_first(node_link* item);
	void add_from_node_last(node_link* item);
	void add_from_node_after(node_link* item, node_link* pos);
	void add_from_node_before(node_link* item, node_link* pos);
	void remove_from_node(node_link* item);
	void replace_from_node(node_link* item, node_link* new_item);
	public:
	void delete_all_from_node();
	node_link* get_first_from_node() const;
	node_link* get_last_from_node() const;
	node_link* get_next_from_node(node_link* pos) const;
	node_link* get_prev_from_node(node_link* pos) const;
	int get_from_node_count();
	void move_from_node_first(node_link* item);
	void move_from_node_last(node_link* item);
	void move_from_node_after(node_link* item, node_link* pos);
	void move_from_node_before(node_link* item, node_link* pos);
	void sort_from_node(int (*compare)(node_link*,node_link*));
	private:
	node_link* _first_to_node ;
	node_link* _last_to_node ;
	int _count_to_node ;
	protected:
	void add_to_node_first(node_link* item);
	void add_to_node_last(node_link* item);
	void add_to_node_after(node_link* item, node_link* pos);
	void add_to_node_before(node_link* item, node_link* pos);
	void remove_to_node(node_link* item);
	void replace_to_node(node_link* item, node_link* new_item);
	public:
	void delete_all_to_node();
	node_link* get_first_to_node() const;
	node_link* get_last_to_node() const;
	node_link* get_next_to_node(node_link* pos) const;
	node_link* get_prev_to_node(node_link* pos) const;
	int get_to_node_count();
	void move_to_node_first(node_link* item);
	void move_to_node_last(node_link* item);
	void move_to_node_after(node_link* item, node_link* pos);
	void move_to_node_before(node_link* item, node_link* pos);
	void sort_to_node(int (*compare)(node_link*,node_link*));
	private:
	super_node* _first_super_node ;
	super_node* _last_super_node ;
	int _count_super_node ;
	protected:
	void add_super_node_first(super_node* item);
	void add_super_node_last(super_node* item);
	void add_super_node_after(super_node* item, super_node* pos);
	void add_super_node_before(super_node* item, super_node* pos);
	void remove_super_node(super_node* item);
	void replace_super_node(super_node* item, super_node* new_item);
	public:
	void delete_all_super_node();
	super_node* get_first_super_node() const;
	super_node* get_last_super_node() const;
	super_node* get_next_super_node(super_node* pos) const;
	super_node* get_prev_super_node(super_node* pos) const;
	int get_super_node_count();
	void move_super_node_first(super_node* item);
	void move_super_node_last(super_node* item);
	void move_super_node_after(super_node* item, super_node* pos);
	void move_super_node_before(super_node* item, super_node* pos);
	void sort_super_node(int (*compare)(super_node*,super_node*));
	private:
	level* _ref_active_level ;
	public:
	inline level* get_active_level() const;
	private:
	level* _ref_level ;
	node* _prev_level ;
	node* _next_level ;
	public:
	inline level* get_level() const;
	size_t _index ;
	protected:
	static size_t _index_node ;
	public:
	inline const size_t& get_index() const;
	node(level* ptr_level, size_t index);
	virtual bool save_relations(std::fstream& f);
	virtual bool load_relations(std::fstream& f);
	node* candidate();
	bool is_candidate();
	void activate();
	bool is_from(node* p_node);
	super_node* register_super_node();
	virtual void who();
	node(level* ptr_level);
	// {user.inside.last.class node.begin}
	// {user.inside.last.class node.end}
};

// {user.after.class node.begin}
// {user.after.class node.end}

#endif //NODE_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_NODE)
#define INCLUDE_INLINES_NODE



inline node::from_node_iterator& node::from_node_iterator::operator =(const node::from_node_iterator& iter)
{
	
	    _iter_to_node = iter._iter_to_node;
	    _ref_from_node    = iter._ref_from_node;
	    _prev_from_node   = iter._prev_from_node;
	    _next_from_node   = iter._next_from_node;
	    _method = iter._method;
	    return *this;
}


inline node_link* node::from_node_iterator::operator ++()
{
	
	    _next_from_node = _iter_to_node->get_next_from_node(_next_from_node);
	    if ( _method != nullptr )
	    {
	        while (_next_from_node != nullptr && !(_next_from_node->*_method)())
	        {
	            _next_from_node = _iter_to_node->get_next_from_node(_next_from_node);
	        }
	    }
	    _ref_from_node = _prev_from_node = _next_from_node;
	    return _ref_from_node;
}


inline node_link* node::from_node_iterator::operator --()
{
	
	    _prev_from_node = _iter_to_node->get_prev_from_node(_prev_from_node);
	    if (_method != 0)
	    {
	        while (_prev_from_node && !(_prev_from_node->*_method)())
	        {
	            _prev_from_node = _iter_to_node->get_prev_from_node(_prev_from_node);
	        }
	    }
	    _ref_from_node = _next_from_node = _prev_from_node;
	    return _ref_from_node;
}


inline  node::from_node_iterator::operator node_link*()
{
	
	    return _ref_from_node;
}


inline node_link* node::from_node_iterator::operator ->()
{
	
	    return _ref_from_node;
}


inline node_link* node::from_node_iterator::get()
{
	
	    return _ref_from_node;
}


inline void node::from_node_iterator::reset()
{
	
	    _ref_from_node = _prev_from_node = _next_from_node = nullptr;
	        
}


inline bool node::from_node_iterator::is_first() const
{
	
	    return (_iter_to_node->get_first_from_node() == _ref_from_node);        
}


inline bool node::from_node_iterator::is_last() const
{
	
	    return (_iter_to_node->get_last_from_node() == _ref_from_node);
}


inline node::to_node_iterator& node::to_node_iterator::operator =(const node::to_node_iterator& iter)
{
	
	    _iter_from_node = iter._iter_from_node;
	    _ref_to_node    = iter._ref_to_node;
	    _prev_to_node   = iter._prev_to_node;
	    _next_to_node   = iter._next_to_node;
	    _method = iter._method;
	    return *this;
}


inline node_link* node::to_node_iterator::operator ++()
{
	
	    _next_to_node = _iter_from_node->get_next_to_node(_next_to_node);
	    if ( _method != nullptr )
	    {
	        while (_next_to_node != nullptr && !(_next_to_node->*_method)())
	        {
	            _next_to_node = _iter_from_node->get_next_to_node(_next_to_node);
	        }
	    }
	    _ref_to_node = _prev_to_node = _next_to_node;
	    return _ref_to_node;
}


inline node_link* node::to_node_iterator::operator --()
{
	
	    _prev_to_node = _iter_from_node->get_prev_to_node(_prev_to_node);
	    if (_method != 0)
	    {
	        while (_prev_to_node && !(_prev_to_node->*_method)())
	        {
	            _prev_to_node = _iter_from_node->get_prev_to_node(_prev_to_node);
	        }
	    }
	    _ref_to_node = _next_to_node = _prev_to_node;
	    return _ref_to_node;
}


inline  node::to_node_iterator::operator node_link*()
{
	
	    return _ref_to_node;
}


inline node_link* node::to_node_iterator::operator ->()
{
	
	    return _ref_to_node;
}


inline node_link* node::to_node_iterator::get()
{
	
	    return _ref_to_node;
}


inline void node::to_node_iterator::reset()
{
	
	    _ref_to_node = _prev_to_node = _next_to_node = nullptr;
	        
}


inline bool node::to_node_iterator::is_first() const
{
	
	    return (_iter_from_node->get_first_to_node() == _ref_to_node);        
}


inline bool node::to_node_iterator::is_last() const
{
	
	    return (_iter_from_node->get_last_to_node() == _ref_to_node);
}


inline node::super_node_iterator& node::super_node_iterator::operator =(const node::super_node_iterator& iter)
{
	
	    _iter_node = iter._iter_node;
	    _ref_super_node    = iter._ref_super_node;
	    _prev_super_node   = iter._prev_super_node;
	    _next_super_node   = iter._next_super_node;
	    _method = iter._method;
	    return *this;
}


inline super_node* node::super_node_iterator::operator ++()
{
	
	    _next_super_node = _iter_node->get_next_super_node(_next_super_node);
	    if ( _method != nullptr )
	    {
	        while (_next_super_node != nullptr && !(_next_super_node->*_method)())
	        {
	            _next_super_node = _iter_node->get_next_super_node(_next_super_node);
	        }
	    }
	    _ref_super_node = _prev_super_node = _next_super_node;
	    return _ref_super_node;
}


inline super_node* node::super_node_iterator::operator --()
{
	
	    _prev_super_node = _iter_node->get_prev_super_node(_prev_super_node);
	    if (_method != 0)
	    {
	        while (_prev_super_node && !(_prev_super_node->*_method)())
	        {
	            _prev_super_node = _iter_node->get_prev_super_node(_prev_super_node);
	        }
	    }
	    _ref_super_node = _next_super_node = _prev_super_node;
	    return _ref_super_node;
}


inline  node::super_node_iterator::operator super_node*()
{
	
	    return _ref_super_node;
}


inline super_node* node::super_node_iterator::operator ->()
{
	
	    return _ref_super_node;
}


inline super_node* node::super_node_iterator::get()
{
	
	    return _ref_super_node;
}


inline void node::super_node_iterator::reset()
{
	
	    _ref_super_node = _prev_super_node = _next_super_node = nullptr;
	        
}


inline bool node::super_node_iterator::is_first() const
{
	
	    return (_iter_node->get_first_super_node() == _ref_super_node);        
}


inline bool node::super_node_iterator::is_last() const
{
	
	    return (_iter_node->get_last_super_node() == _ref_super_node);
}


inline level* node::get_active_level() const
{
	return _ref_active_level;
}


inline level* node::get_level() const
{
	return _ref_level;
}


inline const size_t& node::get_index() const
{
	return _index;
}

#endif //(INCLUDE_INLINES_NODE)
#endif //INCLUDE_INLINES


