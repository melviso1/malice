#if !defined(SUPER_LEVEL_H_INCLUDED)
#define SUPER_LEVEL_H_INCLUDED


// {user.before.class super_level.begin}
// {user.before.class super_level.end}

class super_level: public level
{

	// {user.inside.first.class super_level.begin}
	// {user.inside.first.class super_level.end}

	friend class level;
	public:
	virtual ~super_level();
	super_level(level* ptr_level);
	private:
	level* _ref_level ;
	public:
	inline level* get_level() const;
	void __exit__();
	void __init__(level* ptr_level);
	super_node* get_active_node();
	virtual void load_nodes(std::fstream& f);
	virtual void save_nodes(std::fstream& f);
	super_level(level* ptr_level, std::fstream& f);
	super_node* candidate();
	// {user.inside.last.class super_level.begin}
	// {user.inside.last.class super_level.end}
};

// {user.after.class super_level.begin}
// {user.after.class super_level.end}

#endif //SUPER_LEVEL_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_SUPER_LEVEL)
#define INCLUDE_INLINES_SUPER_LEVEL



inline level* super_level::get_level() const
{
	return _ref_level;
}

#endif //(INCLUDE_INLINES_SUPER_LEVEL)
#endif //INCLUDE_INLINES


