#if !defined(SYMBOL_H_INCLUDED)
#define SYMBOL_H_INCLUDED


// {user.before.class symbol.begin}
// {user.before.class symbol.end}

class symbol: public node
{

	// {user.inside.first.class symbol.begin}
	// {user.inside.first.class symbol.end}

	public:
	interface_level* _ref_interface_level ;
	symbol* _parent_interface_level ;
	symbol* _left_interface_level ;
	symbol* _right_interface_level ;
	inline interface_level* get_interface_level() const;
	size_t _identifier ;
	inline const size_t& get_identifier() const;
	void __exit__();
	void __init__(interface_level* ptr_interface_level);
	symbol(interface_level* ptr_interface_level, size_t index, size_t identifier);
	virtual void who();
	virtual ~symbol();
	symbol(interface_level* ptr_interface_level, size_t identifier);
	// {user.inside.last.class symbol.begin}
	// {user.inside.last.class symbol.end}
};

// {user.after.class symbol.begin}
// {user.after.class symbol.end}

#endif //SYMBOL_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_SYMBOL)
#define INCLUDE_INLINES_SYMBOL



inline interface_level* symbol::get_interface_level() const
{
	return _ref_interface_level;
}


inline const size_t& symbol::get_identifier() const
{
	return _identifier;
}

#endif //(INCLUDE_INLINES_SYMBOL)
#endif //INCLUDE_INLINES


